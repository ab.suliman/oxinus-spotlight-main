from odoo import models, fields, api, _
import logging
from odoo.tools import config
from psycopg2.extensions import TransactionRollbackError
from odoo.tools.float_utils import float_is_zero

_logger = logging.getLogger(__name__)


""" 
    customization significantly enhances the subscription management and invoicing capabilities within Odoo. It automates 
    several aspects of the invoicing process, provides flexibility in invoice handling based on specific conditions, 
    and improves the overall efficiency of managing recurring subscriptions and related financial transactions.

    SaleOrderTemplate Model Inherited from: sale.order.template
    New Fields:
    created_invoice_state: Selection field to determine the state of the created invoice (Draft or Posted).
    auto_send_invoice: Boolean to indicate whether the invoice should be sent automatically.
    notification_mail_id: Many2one field linking to a mail template used for notifications related to the invoice.
    
    SaleOrder Model Inherited from: sale.order
    Methods:
    _handle_automatic_invoices: Handles the process of automatic invoice generation for subscriptions.
    _create_recurring_invoice: Creates recurring invoices for subscriptions, with handling for automatic and manual processes.
    validate_and_send_invoice: Sends an invoice with validation.
    _get_invoiceable_lines: Overrides the method to get invoiceable lines, particularly for subscriptions.
"""


class SaleOrderTemplate(models.Model):
    _inherit = 'sale.order.template'

    created_invoice_state = fields.Selection([
        ('draft', 'Draft'),
        ('posted', 'Posted'),
    ], string='Created Invoice State', default='draft')
    auto_send_invoice = fields.Boolean(string="Auto Send Invoice", default=True)
    notification_mail_id = fields.Many2one(comodel_name="mail.template", domain=[('model', '=', 'account.move')],
                                           store=True)


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    def _handle_automatic_invoices(self, auto_commit, invoices):
        """ This method handle the subscription with or without payment token
            it iterates over a collection of orders and performs various actions based on the order's payment
            status. If an order does not have a payment token, it checks if the order's sale order template has
            a created invoice state of 'posted'. If so, it posts the invoice.
            If an order has a payment token, it attempts to execute the payment using the token. If the payment fails
            due to a missing country on the payment token's partner, an error message is logged and the invoice is unlinked.
            If the payment is successful, a success message is posted, and if the payment fails, an error message
            is logged and the invoice is unlinked. Finally, the method returns the remaining invoices after processing.
        """
        Mail = self.env['mail.mail']
        automatic_values = self._get_automatic_subscription_values()
        existing_invoices = invoices
        for order in self:
            invoice = invoices.filtered(lambda inv: inv.invoice_origin == order.name)
            email_context = self._get_subscription_mail_payment_context()
            # Set the contract in exception. If something go wrong, the exception remains.
            order.with_context(mail_notrack=True).write({'payment_exception': True})
            if not order.payment_token_id:
                if order.sale_order_template_id.created_invoice_state == 'posted':
                    invoice.action_post()
            else:
                try:
                    payment_token = order.payment_token_id
                    transaction = None
                    # execute payment
                    if payment_token:
                        if not payment_token.partner_id.country_id:
                            msg_body = 'Automatic payment failed. Contract set to "To Renew". No country specified on payment_token\'s partner'
                            order.message_post(body=msg_body)
                            order.with_context(mail_notrack=True).write(automatic_values)
                            invoice.unlink()
                            existing_invoices -= invoice
                            if auto_commit:
                                self.env.cr.commit()
                            continue
                        transaction = order._do_payment(payment_token, invoice)
                        # commit change as soon as we try the payment, so we have a trace in the payment_transaction table
                        if auto_commit:
                            self.env.cr.commit()
                    # if transaction is a success, post a message
                    if transaction and transaction.state == 'done':
                        order.with_context(mail_notrack=True).write({'payment_exception': False})
                        self._subscription_post_success_payment(invoice, transaction)
                        if auto_commit:
                            self.env.cr.commit()
                    # if no transaction or failure, log error, rollback and remove invoice
                    if transaction and transaction.state != 'done':
                        if auto_commit:
                            # prevent rollback during tests
                            self.env.cr.rollback()
                        order._handle_subscription_payment_failure(invoice, transaction, email_context)
                        existing_invoices -= invoice  # It will be unlinked in the call above
                except Exception:
                    if auto_commit:
                        # prevent rollback during tests
                        self.env.cr.rollback()
                    # we suppose that the payment is run only once a day
                    last_transaction = self.env['payment.transaction'].search(
                        [('reference', 'like', self.client_order_ref or self.name)], limit=1)
                    error_message = "Error during renewal of contract [%s] %s (%s)" \
                                    % (order.id, order.client_order_ref or order.name,
                                       'Payment recorded: %s' % last_transaction.reference
                                       if last_transaction and last_transaction.state == 'done' else 'Payment not recorded')
                    _logger.exception(error_message)
                    mail = Mail.sudo().create({'body_html': error_message, 'subject': error_message,
                                               'email_to': email_context.get('responsible_email'), 'auto_delete': True})
                    mail.send()
                    if invoice.state == 'draft':
                        existing_invoices -= invoice
                        invoice.unlink()

        return existing_invoices

    def _create_recurring_invoice(self, automatic=False, batch_size=30):
        """
        Creates a recurring invoice.
        The method takes two optional parameters: automatic (defaulting to False) and batch_size (defaulting to 30).
        The method starts by converting the automatic parameter to a boolean value and setting the auto_commit variable based on a condition.
        Then, it initializes some variables and performs some queries to retrieve the subscriptions that need to be invoiced.
        Next, it filters and processes each subscription. It checks if the subscription is in a progress stage,
        and if so, it creates an invoice for the subscription. If the invoice creation is successful, it handles
        automatic payment or invoice posting based on the automatic parameter.
        Finally, the method resets some quantities and processes the invoices to be sent. If there are still
        subscriptions to be processed, it triggers the cron job again to continue the processing.
        The method returns the created account moves.
        """
        automatic = bool(automatic)
        auto_commit = automatic and not bool(config['test_enable'] or config['test_file'])
        Mail = self.env['mail.mail']
        today = fields.Date.today()
        stages_in_progress = self.env['sale.order.stage'].search([('category', '=', 'progress')])
        if len(self) > 0:
            all_subscriptions = self.filtered(
                lambda so: so.is_subscription and so.subscription_management != 'upsell' and not so.payment_exception)
            need_cron_trigger = False
        else:
            search_domain = self._recurring_invoice_domain()
            all_subscriptions = self.search(search_domain, limit=batch_size + 1)
            need_cron_trigger = len(all_subscriptions) > batch_size
            if need_cron_trigger:
                all_subscriptions = all_subscriptions[:batch_size]
        if not all_subscriptions:
            return self.env['account.move']
        # don't spam sale with assigned emails.
        all_subscriptions = all_subscriptions.with_context(mail_auto_subscribe_no_notify=True)
        auto_close_subscription = all_subscriptions.filtered_domain([('end_date', '!=', False)])
        all_invoiceable_lines = all_subscriptions.with_context(recurring_automatic=automatic)._get_invoiceable_lines(
            final=False)

        auto_close_subscription._subscription_auto_close_and_renew()
        if automatic:
            all_subscriptions.write({'is_invoice_cron': True})
        lines_to_reset_qty = self.env[
            'sale.order.line']  # qty_delivered is set to 0 after invoicing for some categories of products (timesheets etc)
        account_moves = self.env['account.move']
        # Set quantity to invoice before the invoice creation. If something goes wrong, the line will appear as "to invoice"
        # It prevent to use the _compute method and compare the today date and the next_invoice_date in the compute.
        # That would be bad for perfs
        all_invoiceable_lines._reset_subscription_qty_to_invoice()
        if auto_commit:
            self.env.cr.commit()
        for subscription in all_subscriptions:
            if subscription.stage_id not in stages_in_progress:
                continue
            try:
                subscription = subscription[
                    0]  # Trick to not prefetch other subscriptions, as the cache is currently invalidated at each iteration
                # in rare occurrences (due to external issues not related with Odoo), we may have
                # our crons running on multiple workers thus doing work in parallel
                # to avoid processing a subscription that might already be processed
                # by a different worker, we check that it has not already been set to "in exception"
                if subscription.payment_exception:
                    continue
                if auto_commit:
                    self.env.cr.commit()  # To avoid a rollback in case something is wrong, we create the invoices one by one
                invoiceable_lines = all_invoiceable_lines.filtered(lambda l: l.order_id.id == subscription.id)
                invoice_is_free = float_is_zero(sum(invoiceable_lines.mapped('price_subtotal')),
                                                precision_rounding=subscription.currency_id.rounding)
                if not invoiceable_lines or invoice_is_free:
                    # We still update the next_invoice_date if it is due
                    if not automatic or subscription.next_invoice_date < today:
                        subscription._update_next_invoice_date()
                        if invoice_is_free:
                            subscription._subscription_post_success_free_renewal()
                    if auto_commit:
                        self.env.cr.commit()
                    continue
                try:
                    invoice = subscription.with_context(recurring_automatic=automatic)._create_invoices()
                    if subscription.sale_order_template_id.notification_mail_id:
                        invoice.with_context().message_post_with_template(
                            subscription.sale_order_template_id.notification_mail_id.id, auto_commit=auto_commit)
                    lines_to_reset_qty |= invoiceable_lines
                except Exception as e:
                    if auto_commit:
                        self.env.cr.rollback()
                    elif isinstance(e, TransactionRollbackError):
                        # the transaction is broken we should raise the exception
                        raise
                    # we suppose that the payment is run only once a day
                    email_context = subscription._get_subscription_mail_payment_context()
                    error_message = _("Error during renewal of contract %s (Payment not recorded)", subscription.name)
                    _logger.exception(error_message)
                    mail = Mail.sudo().create({'body_html': error_message, 'subject': error_message,
                                               'email_to': email_context['responsible_email'], 'auto_delete': True})
                    mail.send()
                    continue
                if auto_commit:
                    self.env.cr.commit()
                # Handle automatic payment or invoice posting
                if automatic:
                    existing_invoices = subscription._handle_automatic_invoices(auto_commit, invoice)
                    account_moves |= existing_invoices
                else:
                    account_moves |= invoice
                subscription.with_context(mail_notrack=True).write({'payment_exception': False})
            except Exception as error:
                _logger.exception("Error during renewal of contract %s",
                                  subscription.client_order_ref or subscription.name)
                if auto_commit:
                    self.env.cr.rollback()
                if not automatic:
                    raise error
            else:
                if auto_commit:
                    self.env.cr.commit()
        lines_to_reset_qty._reset_subscription_quantity_post_invoice()
        all_subscriptions._process_invoices_to_send(account_moves, auto_commit)
        # There is still some subscriptions to process. Then, make sure the CRON will be triggered again asap.
        if need_cron_trigger:
            if config['test_enable'] or config['test_file']:
                # Test environnement: we launch the next iteration in the same thread
                self.env['sale.order']._create_recurring_invoice(automatic, batch_size)
            else:
                self.env.ref('sale_subscription.account_analytic_cron_for_invoice')._trigger()

        if automatic and not need_cron_trigger:
            cron_subs = self.search([('is_invoice_cron', '=', True)])
            cron_subs.write({'is_invoice_cron': False})

        if not need_cron_trigger:
            failing_subscriptions = self.search([('is_batch', '=', True)])
            failing_subscriptions.write({'is_batch': False})

        return account_moves

    def validate_and_send_invoice(self, auto_commit, invoice):
        """
        Validate and send an invoice.
        It takes three parameters: self, auto_commit, and invoice.
        The purpose of this method is to validate and send an invoice. It does the following:
        Calls the ensure_one() method on self to ensure that there is only one record in the current environment.
        Creates an email_context dictionary by copying the current environment context and adding additional key-value pairs related to the invoice.
        Logs a debug message indicating that an invoice mail is being sent to a specific email address for a specific subscription.
        Checks if an invoice mail template is set and if the auto_send_invoice flag is True in the sale order template. If both conditions are met, it calls the message_post_with_template() method on the invoice object, passing in the invoice mail template ID and the auto_commit parameter.
        Sets the is_move_sent attribute of the invoice object to True.
        Overall, this method validates and sends an invoice email using a template if certain conditions are met.
        """
        self.ensure_one()
        email_context = {**self.env.context.copy(), **{
            'total_amount': invoice.amount_total,
            'email_to': self.partner_id.email,
            'code': self.client_order_ref or self.name,
            'currency': self.pricelist_id.currency_id.name,
            'date_end': self.end_date,
            'mail_notify_force_send': False,
            'no_new_invoice': True}}
        _logger.debug("Sending Invoice Mail to %s for subscription %s", self.partner_id.email, self.id)
        # ARJ TODO master: take the invoice template in the settings
        if self.sale_order_template_id.invoice_mail_template_id and self.sale_order_template_id.auto_send_invoice:
            invoice.with_context(email_context).message_post_with_template(
                self.sale_order_template_id.invoice_mail_template_id.id, auto_commit=auto_commit)
            invoice.is_move_sent = True

    def _get_invoiceable_lines(self, final=False):
        """
        Retrieve the invoiceable lines for the current sale order.
        it retrieves the invoiceable lines for a current sale order. It filters out lines that are marked as 'subscription'.
        It checks various conditions to determine which lines should be included in the invoiceable lines.
        It also handles downpayment lines separately and adds them at the end. Finally, it returns the resulting invoiceable lines.
        """
        date_from = fields.Date.today()
        res = super()._get_invoiceable_lines(final=final)
        res = res.filtered(lambda l: l.temporal_type != 'subscription')
        automatic_invoice = self.env.context.get('recurring_automatic')

        invoiceable_line_ids = []
        downpayment_line_ids = []
        pending_section = None

        for line in self.order_line:
            if line.display_type == 'line_section':
                # Only add section if one of its lines is invoiceable
                pending_section = line
                continue

            time_condition = line.order_id.next_invoice_date and line.order_id.next_invoice_date <= date_from and line.order_id.start_date and line.order_id.start_date <= date_from
            line_condition = time_condition or not automatic_invoice  # automatic mode force the invoice when line are not null
            line_to_invoice = False
            if line in res:
                # Line was already marked as to be invoice
                line_to_invoice = True
            elif line.display_type or line.temporal_type != 'subscription':
                # Avoid invoicing section/notes or lines starting in the future or not starting at all
                line_to_invoice = False
            elif line_condition and line.product_id.invoice_policy == 'order' and line.order_id.state == 'sale':
                # Invoice due lines
                line_to_invoice = True
            elif line_condition and line.product_id.invoice_policy == 'delivery' and (
                    not float_is_zero(line.qty_delivered, precision_rounding=line.product_id.uom_id.rounding)):
                line_to_invoice = True

            if line.is_downpayment and line.price_unit > 0:
                line_to_invoice = True

            if line_to_invoice:
                if line.is_downpayment:
                    # downpayment line must be kept at the end in its dedicated section
                    downpayment_line_ids.append(line.id)
                    continue
                if pending_section:
                    invoiceable_line_ids.append(pending_section.id)
                    pending_section = False
                invoiceable_line_ids.append(line.id)

        return self.env["sale.order.line"].browse(invoiceable_line_ids + downpayment_line_ids)
