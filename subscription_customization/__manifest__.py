# -*- coding: utf-8 -*-
{
    'name': "Subscription Customization",

    'summary': """
        Subscription Customization""",

    'description': """
        Subscription Customization
    """,

    'author': "Forge Solutions",
    'website': "https://www.forge-solutions.com",
    'license': 'Other proprietary',
    

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/16.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale', 'sale_subscription'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/subscription_views_inherit.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
}
