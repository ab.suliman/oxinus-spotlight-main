from odoo import models, fields, api, _
from datetime import timedelta

"""
Extends the helpdesk.ticket model to include phone number fields and Ziwo call history.
"""


class HelpdeskTicket(models.Model):
    _inherit = "helpdesk.ticket"

    partner_mobile = fields.Char(related="partner_id.mobile", string="Mobile")
    partner_parent = fields.Many2one(related="partner_id.parent_id", string="Company")

    ziwo_show_c2c_d2c = fields.Boolean(compute='_compute_ziwo_show_c2c_d2c')

    def _compute_ziwo_show_c2c_d2c(self):
        """hat iterates over a collection of records. For each record, it sets the ziwo_show_c2c_d2c field
        to a list of model names obtained from a search in the ziwo.active.form model."""
        for record in self:
            record.ziwo_show_c2c_d2c = record._name in record.env['ziwo.active.form'].sudo().search([]).mapped(
                'model_id.model')

    # override on create to capture the two see if two context values exist (ziwo_model and ziwo_record)
    @api.model
    def create(self, vals):
        """
           Create a new Helpdesk Ticket record."""

        res = super(HelpdeskTicket, self).create(vals)
        ziwo_model = self.env.context.get('ziwo_model')
        ziwo_record = self.env.context.get('ziwo_record')
        if ziwo_model and ziwo_record:
            self.env['ziwo.history'].sudo().search([('id', '=', ziwo_record)]).update_call_model_reference(
                'helpdesk.ticket', res.id)
        return res

    def action_add_note(self):
        """
        Adds a note to the current object."""

        return {
            "name": "Add Note",
            "type": "ir.actions.act_window",
            "view_mode": "form",
            "target": "new",
            "res_model": "add.note.wizard",
            "context": {"default_ticket_id": self.id},
        }

    def action_view_ticket(self):
        """it updates the call model of a screen called agent.screen with the helpdesk ticket ID. Then, it returns a
         dictionary that represents an action to open a new window displaying the helpdesk ticket form.
         The window will be opened in the current view and will show the form view of the helpdesk.ticket model
         with the ID of the current ticket."""

        self.env['agent.screen'].sudo().update_call_model('helpdesk.ticket', self.id)
        return {
            "name": "View Ticket",
            "type": "ir.actions.act_window",
            "view_mode": "form",
            "views": [[False, "form"]],
            "target": "current",
            "res_model": "helpdesk.ticket",
            "res_id": self.id
        }

    def open_create_call_wizard(self):
        """hat returns a dictionary. The dictionary contains various key-value pairs that define the
        behavior of a window action. The window action is used to open a new form view for creating a
         record of the create.call.wizard model. The context key provides default values for some
         fields in the form view."""

        self.ensure_one()
        return {
            "type": "ir.actions.act_window",
            "res_model": "create.call.wizard",
            "view_mode": "form",
            "view_type": "form",
            "target": "new",
            "context": {
                "default_model": 'helpdesk.ticket',
                "default_record": self.id,
            },
        }

    def create_new_call(self):
        """t calls a method create_new_call on an object agent.screen from the env dictionary.
         It passes three arguments to this method: the string 'helpdesk.ticket', the value of
          self.id, and the value of self.partner_id.mobile_trim. The sudo() method is called
           on the agent.screen object, which gives it elevated privileges.
"""
        self.env['agent.screen'].sudo().create_new_call('helpdesk.ticket', self.id, self.partner_id.mobile_trim)

    def create_new_call_phone(self):
        """
        that creates a new call using the agent.screen model. It passes the parameters
         'helpdesk.ticket', self.id, and self.partner_id.phone_trim to the create_new_call method.
          The sudo method is used to execute the call creation with elevated privileges.
        """

        self.env['agent.screen'].sudo().create_new_call('helpdesk.ticket', self.id, self.partner_id.phone_trim)
