from odoo import models, fields, api
from odoo.http import request

"""A model to override sudo with additional permissions or configurations.
"""


class ForgeSudoOverride(models.TransientModel):
    _name = "forge.sudo.override"
    _description = "Forge Sudo Override"

    @api.model
    def get_param_navigate_outbound_calls_c2c(self):
        """It retrieves a configuration parameter value from the database and returns it."""

        params = self.env['ir.config_parameter'].sudo()
        return params.get_param('forge_ziwo_navigate_outbound_calls_c2c')

    @api.model
    def create_act_window(self, result_action):
        """ It takes a parameter called result_action and creates a new record of the model
        ir.actions.act_window. The method returns the ID of the created record.
        the sudo() method is used to bypass access rights checks."""

        action = self.env['ir.actions.act_window'].sudo().create(result_action)
        return action.id

    @api.model
    def create_client_notification(self, result_action):
        """Inside the method, it creates a new record of the model ir.actions.client with the data
        passed in as result_action. The created record is then returned, specifically its id value.
        The method is used to create client notifications in the application."""

        action = self.env['ir.actions.client'].sudo().create(result_action)
        return action.id
