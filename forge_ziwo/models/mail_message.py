from odoo import models, fields, api

"""
Overrides the mail.message model to allow unsanitized HTML content, probably to include call recordings in messages.
"""


class MailMessage(models.Model):
    _inherit = 'mail.message'

    body = fields.Html(sanitize_attributes=False)
