from odoo import models, fields, api, _
from datetime import timedelta

"""
Extends the project.task model to associate tasks with Ziwo call histories.
"""


class ProjectTask(models.Model):
    _inherit = "project.task"

    partner_parent = fields.Many2one(related="partner_id.parent_id", string="Company")

    ziwo_show_c2c_d2c = fields.Boolean(compute='_compute_ziwo_show_c2c_d2c')

    def _compute_ziwo_show_c2c_d2c(self):
        """updates the ziwo_show_c2c_d2c field for each record in the current object. The field is set to True if the _name of the record exists in the model field of any record in the ziwo.active.form model."""
        for record in self:
            record.ziwo_show_c2c_d2c = record._name in record.env['ziwo.active.form'].sudo().search([]).mapped(
                'model_id.model')

    # override on create to capture the two see if two context values exist (ziwo_model and ziwo_record)
    @api.model
    def create(self, vals):
        """using an API decorator @api.model. The method is used to create a new record in the ProjectTask model. After the record is created, it checks if there are two specific values (ziwo_model and ziwo_record) in the context. If those values exist, it updates a reference in the ziwo.history model with the newly created project.task record. Finally, it returns the newly created record."""
        res = super(ProjectTask, self).create(vals)
        ziwo_model = self.env.context.get('ziwo_model')
        ziwo_record = self.env.context.get('ziwo_record')
        if ziwo_model and ziwo_record:
            self.env['ziwo.history'].sudo().search([('id', '=', ziwo_record)]).update_call_model_reference(
                'project.task', res.id)
        return res

    def action_add_note(self):
        """hat returns a dictionary. The dictionary contains various key-value pairs that define the behavior of an action in a user interface. It specifies that when the action is triggered, a new window should open in the "form" view mode, displaying a wizard for adding a note. The "context" key contains additional data that will be passed to the wizard, including the default value for the "task_id" field, which is set to the value of self.id."""
        return {
            "name": "Add Note",
            "type": "ir.actions.act_window",
            "view_mode": "form",
            "target": "new",
            "res_model": "add.note.wizard",
            "context": {"default_task_id": self.id},
        }

    def action_view_task(self):
        """hat updates the call model of the agent.screen object with the project.task model and the
        current task ID. It then returns a dictionary with information about how to display a window to view the task. The window's title is "View Task" and it should display the task in form view mode"""
        self.env['agent.screen'].sudo().update_call_model('project.task', self.id)
        return {
            "name": "View Task",
            "type": "ir.actions.act_window",
            "view_mode": "form",
            "views": [[False, "form"]],
            "target": "current",
            "res_model": "project.task",
            "res_id": self.id
        }

    def open_create_call_wizard(self):
        """The dictionary contains information about how to open a new window for a wizard form.
        The window will display the form view of the create.call.wizard model, with the "new" target.
        The context of the window will have a default value for the default_model key set
         to "project.task" and the default_record key set to the id of the current object."""

        self.ensure_one()
        return {
            "type": "ir.actions.act_window",
            "res_model": "create.call.wizard",
            "view_mode": "form",
            "view_type": "form",
            "target": "new",
            "context": {
                "default_model": 'project.task',
                "default_record": self.id,
            },
        }

    def create_new_call(self):
        self.env['agent.screen'].sudo().create_new_call('project.task', self.id, self.partner_id.mobile_trim)

    def create_new_call_phone(self):
        """It calls a method create_new_call on an object agent.screen. The create_new_call method is
         passed three arguments: the model name 'project.task', the id of the current object, and the
         phone_trim attribute of the partner_id object. The create_new_call method is executed with
          elevated privileges (using sudo())."""

        self.env['agent.screen'].sudo().create_new_call('project.task', self.id, self.partner_id.phone_trim)
