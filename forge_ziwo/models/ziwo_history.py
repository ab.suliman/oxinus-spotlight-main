from odoo import models, fields, api
from odoo.exceptions import UserError
from datetime import datetime
import requests, base64, json

"""
Manages the history of calls made through Ziwo, including recording links, call types, and statuses.
"""


class ZiwoHistory(models.Model):
    _name = 'ziwo.history'
    _description = 'Ziwo Call History'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    agent_id = fields.Many2one('res.users', string='Agent', readonly=1)
    agent_ids = fields.Many2many('res.users', string='Attending Agents', readonly=1)
    partner_id = fields.Many2one('res.partner', string='Contact', readonly=1)
    partner_parent = fields.Many2one(related="partner_id.parent_id", string="Company")
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.company)
    mobile = fields.Char(string='Mobile Number', readonly=1)
    call_type = fields.Selection([
        ('inbound', 'Inbound'),
        ('outbound', 'Outbound')
    ], string='Call Type', readonly=1)
    call_status = fields.Selection([
        ('answered', 'Answered'),
        ('rejected', 'Rejected'),
        ('missed', 'Missed')
    ], string='Call Status', readonly=1)
    call_id = fields.Char(string='Call ID', readonly=1)
    call_time = fields.Datetime(string='Call Time', readonly=1)
    model_reference = fields.Reference(string='Model', selection='_get_model_selection')
    model_reference_model = fields.Char(string='Model', compute='_compute_model_reference')
    model_reference_record = fields.Char(string='Record', compute='_compute_record_reference', tracking=True)
    main_message_id = fields.Many2one('mail.message', string='Message')
    main_message_id_update = fields.Many2one('mail.message', string='Message')
    chatter_message_id = fields.Many2one('mail.message', string='Chatter Message')
    chatter_message_id_update = fields.Many2one('mail.message', string='Chatter Message')
    chatter_message_model = fields.Char(string='Chatter Message Model', related='chatter_message_id.model')
    chatter_message_record_id = fields.Many2oneReference(string='Chatter Message Record',
                                                         related='chatter_message_id.res_id')
    chatter_message_subject = fields.Char(string='Chatter Message Subject', related='chatter_message_id.subject')
    call_recording = fields.Char(string='Call Recording', readonly=1)
    call_recording_html = fields.Html(string='Call Recording', compute='_compute_call_recording_html',
                                      sanitize_attributes=False, readonly=1)
    call_transfer = fields.Boolean(string='Transfer', default=False, readonly=1)
    call_transfer_type = fields.Selection([
        ('blind', 'Blind'),
        ('attend', 'Attend')
    ], string='Transfer Type', readonly=1)

    @api.depends('model_reference')
    def _compute_model_reference(self):
        """
        method will be automatically called whenever the field model_reference changes. Inside the method,
        it iterates over each record and checks if the model_reference field has a value. If it does, it uses the
        value to search for a record in the ir.model model and retrieves its name. The name is then assigned
        to the model_reference_model field of the current record. If model_reference is empty,
        model_reference_model is set to False.
        """
        for record in self:
            if record.model_reference:
                model_name = self.env['ir.model'].sudo().search([('model', '=', record.model_reference._name)]).name
                record.model_reference_model = model_name
            else:
                record.model_reference_model = False

    @api.depends('model_reference')
    def _compute_record_reference(self):
        """
        whenever the model_reference field is changed. Inside the method, it checks if model_reference has a value.
        If it does, it searches for the corresponding model and record name using the _name and id attributes of
        model_reference. It then sets the value of model_reference_record field as a combination of the model name
        and record name. If model_reference is empty, it sets model_reference_record to False.
        """

        for record in self:
            if record.model_reference:
                model_name = self.env['ir.model'].sudo().search([('model', '=', record.model_reference._name)]).name
                record_name = self.env[record.model_reference._name].sudo().search(
                    [('id', '=', record.model_reference.id)]).display_name
                record.model_reference_record = model_name + ", " + record_name
            else:
                record.model_reference_record = False

    @api.model
    def _get_model_selection(self):
        """
        method that returns the selection of models that can be used in the active form.
        """
        selection = []
        active_forms = self.env['ziwo.active.form'].sudo().search([]).mapped('model_id')
        for model in active_forms:
            selection.append((model.model, model.name))
        return selection

    @api.depends('call_recording')
    def _compute_call_recording_html(self):
        """
        takes in a record as an argument. It checks if the call_recording field of the record is not empty and if so, it sets
        the call_recording_html field of the record to an HTML audio player with the corresponding audio file as the source.
        If the call_recording field is empty, it sets the call_recording_html field to False.
        """
        for record in self:
            if record.call_recording:
                record.call_recording_html = '<audio name="media" controls="true" id="%s"><source src="%s" type="audio/mpeg"></audio>' % (
                    record.call_id, record.call_recording)
            else:
                record.call_recording_html = False

    def action_update_call_recording(self):
        message = {
            'action': 'get',
            'record': self.id,
        }
        self.env['bus.bus'].sudo()._sendone(self.env.user.partner_id, 'ziwo/bus', message)

    @api.model
    def create_call_recording(self, id, session):
        """
        The method takes in two parameters, id and session. It performs several operations to retrieve and process a call
        recording from an external API and updates the call_recording field of a record in the database. It also handles
        error scenarios and returns a dictionary with an action to display a notification to the user
        """
        record = self.browse(id)

        if not session:
            raise UserError('ZIWO session expired. Please refresh the page.')

        admin_auth_token = self.env['ir.config_parameter'].sudo().get_param('forge_ziwo_admin_auth_token')

        instance_name = session['accountName']
        access_token = session['token']

        base_url = 'https://%s-api.aswat.co' % instance_name
        endpoint = f'/callHistory/{record.call_id}/recording/signed-url'

        if admin_auth_token:
            headers = {'access_token': admin_auth_token}
        else:
            headers = {'access_token': access_token}

        response = requests.get(base_url + endpoint, headers=headers)

        return_action = {}
        action_type = 'success'
        action_message = 'Call recording updated successfully'

        if response.status_code == 200:
            try:
                record.call_recording = json.loads(response.content.decode('utf-8'))['content']['url']
            except:
                action_type = 'danger'
                action_message = 'Error processing call recording'

        elif record.call_status == 'answered':
            error_message = json.loads(response.content.decode('utf-8'))['error']['message']
            action_type = 'danger'
            action_message = error_message

        if action_type == 'success' and record.call_recording and record.main_message_id:
            record.create_message(False, True)

        return {
            'action': {
                'type': 'ir.actions.client',
                'name': 'Call Recording Notification',
                'tag': 'display_notification',
                'params': {
                    'message': action_message,
                    'type': action_type,
                    'sticky': False,
                }
            }
        }

    def create_message(self, duplicate=False, update=False):
        """
    that takes two optional boolean parameters: duplicate and update.
    The method creates a message using the mail.message model. It constructs the message body by iterating over a list of
    field names and retrieving their corresponding values from the self object. It formats the field name and value as
    HTML and appends them to the body string. If the call_recording field is present, it also adds an audio player
    to the message body.
    Finally, the method creates a new mail.message record with the subject and body, and sets some additional
    fields based on the values of update and duplicate. It then returns True.
    Please note that the self object is not defined in the code snippet, so it's not possible to determine the exact
    context of this code. If you provide more information about the self object or the purpose of this code,
    I can provide a more specific explanation.

        """
        subject = 'New call created'
        body = ''
        is_transfer_flag = False
        for field_name in ['agent_id', 'call_transfer', 'agent_ids', 'partner_id', 'mobile', 'call_type', 'call_time',
                           'call_status']:
            field_value = self[field_name]
            field_display_name = self._fields[field_name].string
            if field_name in ['agent_id', 'partner_id']:
                field_value = field_value.display_name
            elif field_name == 'call_transfer':
                if not field_value:
                    continue
                is_transfer_flag = True
                field_value = 'Yes'
            elif field_name == 'agent_ids':
                if not is_transfer_flag:
                    continue
                field_value = ', '.join(field_value.mapped('display_name'))
            body += '<p><strong>%s:</strong> %s</p>' % (field_display_name, field_value)
        if self.call_recording:
            body += '<p><audio name="media" controls="true" id="%s"><source src="%s" type="audio/mpeg"></audio></p>' % (
                self.call_id, self.call_recording)
        message = self.env['mail.message'].sudo().create({
            'model': self._name,
            'res_id': self.id,
            'subject': subject,
            'body': body,
        })

        if update:
            if not self.main_message_id_update:
                self.main_message_id_update = message.id
            if self.model_reference and not self.chatter_message_id_update:
                self.chatter_message_id_update = message.id
                self.chatter_message_id_update.write({
                    'model': self.model_reference._name,
                    'res_id': self.model_reference.id,
                })
        else:
            if not duplicate:
                self.main_message_id = message.id
            else:
                self.chatter_message_id = message.id

        return True

    def action_apply_model(self):
        if not self.model_reference:
            raise UserError('Model or record cannot be empty')

        if not self.chatter_message_id and self.main_message_id:
            self.create_message(True)

        self.chatter_message_id.write({
            'model': self.model_reference._name,
            'res_id': self.model_reference.id,
        })

        return True

    @api.model
    def create_record(self, mobile, call_type, call_id, call_time, model_name, record_id):
        """
        This method creates a new record based on the provided parameters. It searches for an existing partner based on a
        mobile number, and if not found, creates a new partner. It then converts the call time to a string format, creates
        a dictionary with the necessary data for the new record, and finally creates and returns the new record.
        :param mobile:
        :param call_type:
        :param call_id:
        :param call_time:
        :param model_name:
        :param record_id:
        :return:
        """
        partner_id = self.env['res.partner'].sudo().search(
            ['|', ('mobile_trim', '=', mobile), ('phone_trim', '=', mobile)])
        if not partner_id:
            partner_id = self.env['res.partner'].sudo().create({
                'name': mobile,
                'mobile': mobile,
            })

        date_time = fields.Datetime.to_string(datetime.strptime(call_time, '%Y-%m-%dT%H:%M:%S.%fZ'))

        new_record_data = {
            'agent_id': self.env.user.id,
            'agent_ids': [(6, 0, [self.env.user.id])],
            'partner_id': partner_id.id,
            'mobile': mobile,
            'call_type': call_type,
            'call_id': call_id,
            'call_time': date_time,
        }

        if model_name and record_id:
            new_record_data['model_reference'] = '%s,%s' % (model_name, record_id) if model_name in self.env[
                'ziwo.active.form'].sudo().search([]).mapped('model_id.model') else False

        new_record = self.create(new_record_data)

        return new_record

    @api.model
    def create_call_record(self, mobile, call_type, call_id, call_time, model_name, record_id):
        new_record = self.create_record(mobile, call_type, call_id, call_time, model_name, record_id)
        action = new_record.send_to_agent()
        return {
            'record_id': new_record.id,
            'action': action,
        }

    @api.model
    def transfer_call_record(self, parent_call_id, transfer_type):
        """
        It takes two parameters: parent_call_id and transfer_type.
        The method searches for a record in the ziwo.history model with a matching call_id. If a record is found, it updates
        the call_transfer and call_transfer_type fields of the record based on the transfer_type parameter.
        If the transfer_type is 'blind', it sets the agent_id field of the record to the current user's ID.
         If the transfer_type is 'attend', it adds the current user's ID to the agent_ids field of the record.
        Finally, it returns the ID of the record.
        """
        search_parent_call = self.env['ziwo.history'].sudo().search([('call_id', '=', parent_call_id)])
        if search_parent_call:
            search_parent_call.call_transfer = True
            search_parent_call.call_transfer_type = transfer_type
            if transfer_type == 'blind':
                search_parent_call.agent_id = self.env.user.id
            elif transfer_type == 'attend':
                search_parent_call.agent_ids = [(6, 0, [self.env.user.id])]
        return search_parent_call.id

    @api.model
    def update_call_id(self, id, call_id, call_time):
        record = self.browse(id)
        record.call_id = call_id
        record.call_time = fields.Datetime.to_string(datetime.strptime(call_time, '%Y-%m-%dT%H:%M:%S.%fZ'))
        return record.id

    @api.model
    def update_call_status(self, id, call_status):
        record = self.browse(id)
        if record.call_status == False:
            record.call_status = call_status
        return record.id

    def update_call_model_reference(self, model_name, record_id):
        self.model_reference = '%s,%s' % (model_name, record_id) if model_name in self.env[
            'ziwo.active.form'].sudo().search([]).mapped('model_id.model') else False
        self.action_apply_model()

    @api.model
    def update_call_model(self, id, model_name, record_id, context=False, receive_model=False, receive_record=False):
        """
        The method takes several parameters and performs a series of conditional operations based on the values of those
        parameters and some configuration settings. At the end, it returns the id of a record. The exact purpose
        and behavior of this method would depend on its usage in the context of the application.
        """
        record = self.browse(id)
        if not context:
            if model_name and record_id:
                record.model_reference = '%s,%s' % (model_name, record_id) if model_name in self.env[
                    'ziwo.active.form'].sudo().search([]).mapped('model_id.model') else False
        else:
            active_history_bool = self.env['ir.config_parameter'].sudo().get_param('forge_ziwo_active_form_history')
            if active_history_bool and receive_model:
                record.model_reference = '%s,%s' % (receive_model, receive_record) if receive_model in self.env[
                    'ziwo.active.form'].sudo().search([]).mapped('model_id.model') else False
            else:
                active_form_id = self.env['ir.config_parameter'].sudo().get_param('forge_ziwo_active_form_id')
                if active_form_id:
                    if self.env['ziwo.active.form'].sudo().search_count([('id', '=', int(active_form_id))]) > 0:
                        active_form = self.env['ziwo.active.form'].sudo().browse(int(active_form_id))
                        if active_form.action == 'new' and active_form.model_id.model != 'res.partner':
                            default_inputs = {
                                'default_partner_id': record.partner_id.id,
                                'default_user_id': self.env.user.id,
                                'default_team_id': self.env.user.sale_team_id.id,
                                'ziwo_model': 'ziwo.history',
                                'ziwo_record': record.id,
                            }
                            name = "%s's %s" % (record.partner_id.name, active_form.model_id.name)
                            sub_view_id = False

                            if active_form.model_id.model == 'sale.order':
                                default_inputs['default_is_subscription'] = 1
                                default_inputs['default_subscription_management'] = 'create'
                                sub_view_id = self.env.ref('sale_subscription.sale_subscription_primary_form_view').id
                                name = "%s's %s" % (record.partner_id.name, "Subscription")

                            required_fields = self.env['ir.model.fields'].sudo().search(
                                [('model_id', '=', active_form.model_id.id), ('required', '=', True)])

                            action = {
                                'name': name,
                                'type': 'ir.actions.act_window',
                                'res_model': active_form.model_id.model,
                                'view_mode': 'form',
                                'view_id': sub_view_id,
                                'context': default_inputs,
                                'target': 'new',
                            }

                            return {
                                'record_id': record.id,
                                'action': action,
                            }

                        elif active_form.action == 'existing' and active_form.model_id.model == 'res.partner':
                            search_model = self.env[active_form.model_id.model].sudo().search(
                                [('id', '=', record.partner_id.id)])
                            if search_model:
                                record.model_reference = '%s,%s' % (
                                    active_form.model_id.model, search_model.id) if active_form.model_id.model in \
                                                                                    self.env[
                                                                                        'ziwo.active.form'].sudo().search(
                                                                                        []).mapped(
                                                                                        'model_id.model') else False
        return record.id

    @api.model
    def create_call_message(self, id):
        record = self.browse(id)
        record.create_message()
        if record.model_reference:
            record.action_apply_model()
        return record.id

    def send_to_agent(self):
        if self.env['ir.config_parameter'].sudo().get_param('forge_ziwo_navigate_%s_calls' % self.call_type):
            return {
                'type': 'ir.actions.act_window',
                'name': 'Agent Screen',
                'res_model': 'agent.screen',
                'view_mode': 'form',
                'target': 'current',
                'context': {'default_partner_id': self.partner_id.id, 'form_view_initial_mode': 'edit',
                            'no_breadcrumbs': True},
            }

    @api.model
    def set_agent_screen(self, id):
        record = self.browse(id)
        return {
            'type': 'ir.actions.act_window',
            'name': 'Agent Screen',
            'res_model': 'agent.screen',
            'view_mode': 'form',
            'target': 'current',
            'context': {'default_partner_id': record.partner_id.id, 'form_view_initial_mode': 'edit',
                        'no_breadcrumbs': True},
        }

    def action_add_note(self):
        return {
            "name": "Add Note",
            "type": "ir.actions.act_window",
            "view_mode": "form",
            "target": "new",
            "res_model": "add.note.wizard",
            "context": {"default_ziwo_history_id": self.id},
        }

    def action_view_call(self):
        return {
            "name": "View Call",
            "type": "ir.actions.act_window",
            "view_mode": "form",
            "views": [[False, "form"]],
            "target": "current",
            "res_model": "ziwo.history",
            "res_id": self.id
        }
