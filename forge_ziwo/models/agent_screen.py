from odoo import models, fields, api
from datetime import timedelta

"""
    This model is used to display the agent screen in the web client.
    which is likely a UI component for agents to manage calls, opportunities, tasks, tickets, and other records associated with a contact.
    """


class AgentScreen(models.TransientModel):
    _name = "agent.screen"
    _description = "Agent Screen"

    name = fields.Char(default="Agent Screen")

    partner_id = fields.Many2one("res.partner", string="Contact")

    phone = fields.Char(string="Phone", related="partner_id.phone")
    mobile = fields.Char(string="Mobile", related="partner_id.mobile")
    email = fields.Char(string="Email", related="partner_id.email")
    company = fields.Char(string="Company", related="partner_id.parent_id.name")
    country = fields.Char(string="Country", related="partner_id.country_id.name")
    tz = fields.Selection(string="Timezone", related="partner_id.tz")
    tz_offset = fields.Char(string="Timezone", related="partner_id.tz_offset")

    show_company_filter = fields.Boolean(string="Show Company Filter", compute="_compute_show_company_filter",
                                         readonly=True)
    enable_company_filter = fields.Boolean(string="Enable Company Filter", default=False, readonly=True)

    @api.depends('partner_id')
    def _compute_show_company_filter(self):
        """
        Compute the value of the 'show_company_filter' field based on the 'partner_id' field.

        Parameters:
            self (Model): The current record or records on which the computation is being performed.

        Returns:
            None
        """
        for record in self:
            record["show_company_filter"] = True if record.partner_id.parent_id else False

    def toggle_commpany_filter(self):
        """
        Toggles the company filter for the current record.
        that toggles the value of the enable_company_filter attribute. If the attribute is currently True, it will be
        set to False, and if it's False, it will be set to True. The method is called on an instance of a class.
        """
        self.ensure_one()
        if self.enable_company_filter:
            self.enable_company_filter = False
        else:
            self.enable_company_filter = True

    # opportunity_ids = fields.One2many(
    #     related="partner_id.opportunity_ids",
    #     string="Opportunities",
    #     readonly=1,
    #     groups="sales_team.group_sale_salesman"
    # )
    opportunity_ids = fields.Many2many(
        comodel_name='crm.lead',
        string='Opportunities',
        compute='_compute_many2many',
    )
    # ticket_ids = fields.One2many(
    #     related="partner_id.ticket_ids",
    #     string="Tickets",
    #     readonly=1,
    #     groups="helpdesk.group_helpdesk_user"
    # )
    ticket_ids = fields.Many2many(
        comodel_name='helpdesk.ticket',
        string='Tickets',
        compute='_compute_many2many',
    )
    # subscription_ids = fields.One2many(
    #     related="partner_id.subscription_ids",
    #     string="Subscriptions",
    #     readonly=1,
    #     groups="sales_team.group_sale_salesman"
    # )
    subscription_ids = fields.Many2many(
        comodel_name='sale.order',
        string='Subscriptions',
        compute='_compute_many2many',
    )
    # task_ids = fields.One2many(
    #     related="partner_id.task_ids",
    #     string="Tickets",
    #     readonly=1,
    #     groups="project.group_project_user"
    # )
    task_ids = fields.Many2many(
        comodel_name='project.task',
        string='Tasks',
        compute='_compute_many2many',
    )
    # calls_ids = fields.One2many(
    #     related="partner_id.calls_ids",
    #     string="Calls",
    #     readonly=1,
    # )
    calls_ids = fields.Many2many(
        comodel_name='ziwo.history',
        string='Calls',
        compute='_compute_many2many',
    )

    @api.depends('partner_id', 'show_company_filter', 'enable_company_filter')
    def _compute_many2many(self):
        """
        Compute the many-to-many relation fields for the current record.

        This function is triggered whenever the 'partner_id', 'show_company_filter', or 'enable_company_filter'
        fields are modified.

        defines a method _compute_many2many that is triggered whenever the fields partner_id, show_company_filter,
        or enable_company_filter are modified. Inside the method, it sets the values of several fields
        (subscription_ids, task_ids, etc.) to an empty list, and then it performs searches on different models
        (sale.order, project.task, etc.) based on certain conditions and assigns the search results to the respective fields.

        """
        for record in self:
            record["subscription_ids"] = record["task_ids"] = record["calls_ids"] = record["ticket_ids"] = record[
                "opportunity_ids"] = [(6, 0, [])]
            if record.partner_id:
                domain = [('company_id', '=', self.env.user.company_id.id)]
                if record.show_company_filter and record.enable_company_filter:
                    domain.append(('partner_parent', '=', record.partner_id.parent_id.id))
                else:
                    domain.append(('partner_id', '=', record.partner_id.id))
                print(domain)
                record["subscription_ids"] = self.env["sale.order"].sudo().search(domain)
                record["task_ids"] = self.env["project.task"].sudo().search(domain)
                record["calls_ids"] = self.env["ziwo.history"].sudo().search(domain)
                record["ticket_ids"] = self.env["helpdesk.ticket"].sudo().search(domain)
                record["opportunity_ids"] = self.env["crm.lead"].sudo().search(domain)

    opportunity_count = fields.Integer(string='Total:', compute='_compute_count')
    ticket_count = fields.Integer(string='Total:', compute='_compute_count')
    subscription_count = fields.Integer(string='Total:', compute='_compute_count')
    task_count = fields.Integer(string='Total:', compute='_compute_count')
    calls_count = fields.Integer(string='Total:', compute='_compute_count')

    @api.depends('opportunity_ids', 'ticket_ids', 'subscription_ids', 'task_ids', 'calls_ids')
    def _compute_count(self):
        """
        Compute the count of related records.

        This function is a decorator for the `_compute_count` method and it is called whenever the fields
        `opportunity_ids`, `ticket_ids`, `subscription_ids`, `task_ids`, or `calls_ids` are modified.

        Parameters:
            self (RecordSet): The current recordset.

        Returns:
            None

        Side Effects:
            - Updates the `opportunity_count` field with the count of related `opportunity_ids`.
            - Updates the `ticket_count` field with the count of related `ticket_ids`.
            - Updates the `subscription_count` field with the count of related `subscription_ids`.
            - Updates the `task_count` field with the count of related `task_ids`.
            - Updates the `calls_count` field with the count of related `calls_ids`.
        """
        for record in self:
            record["opportunity_count"] = len(record["opportunity_ids"]) if record.opportunity_ids else 0
            record["ticket_count"] = len(record["ticket_ids"]) if record.ticket_ids else 0
            record["subscription_count"] = len(record["subscription_ids"]) if record.subscription_ids else 0
            record["task_count"] = len(record["task_ids"]) if record.task_ids else 0
            record["calls_count"] = len(record["calls_ids"]) if record.calls_ids else 0

    show_oppotunity_tab = fields.Boolean(
        string="Show Opportunity",
        default=lambda self: 'crm.lead' in self.env['ziwo.active.form'].sudo().search([]).mapped('model_id.model')
    )
    show_ticket_tab = fields.Boolean(
        string="Show Ticket",
        default=lambda self: 'helpdesk.ticket' in self.env['ziwo.active.form'].sudo().search([]).mapped(
            'model_id.model')
    )
    show_subscription_tab = fields.Boolean(
        string="Show Subscription",
        default=lambda self: 'sale.order' in self.env['ziwo.active.form'].sudo().search([]).mapped('model_id.model')
    )
    show_task_tab = fields.Boolean(
        string="Show Task",
        default=lambda self: 'project.task' in self.env['ziwo.active.form'].sudo().search([]).mapped('model_id.model')
    )

    def open_edit_contact_wizard(self):
        """
        Opens the edit contact wizard.

        The method takes in the current object as a parameter and returns a dictionary representing the action to be performed.
        Inside the method, it first ensures that there is only one record (self.ensure_one()). Then,
        if self.partner_id exists, it creates a new instance of the edit.contact.wizard model,
        passing in the partner_id from the current object.
        Finally, it returns a dictionary specifying the type of action to be performed, the model to be used,
        the view mode and type, the ID of the wizard instance, and the target window for the action.

        :param self: The current object.
        :return: A dictionary representing the action to be performed.
        """
        self.ensure_one()
        if self.partner_id:
            wizard = self.env["edit.contact.wizard"].sudo().create({
                "partner_id": self.partner_id.id,
            })
            return {
                "type": "ir.actions.act_window",
                "res_model": "edit.contact.wizard",
                "view_mode": "form",
                "view_type": "form",
                "res_id": wizard.id,
                "target": "new",
            }

    def create_new_opportunity(self):
        """
        create_new_opportunity in a class. When called, it updates the call model of the agent.screen object
        to 'crm.lead'. It then returns a dictionary that represents an action to create a new opportunity.
        The dictionary contains various key-value pairs that define the behavior of the action, such as the name,
        type, and model of the window to be displayed, as well as default values for certain fields.
        The action is intended to be executed in the current window.
        """
        self.env['agent.screen'].sudo().update_call_model('crm.lead')
        return {
            'name': 'Create New Opportunity',
            'type': 'ir.actions.act_window',
            'res_model': 'crm.lead',
            'view_mode': 'form',
            'context': {
                'default_partner_id': self.partner_id.id,
                'default_user_ids': self.env.user.id,
                'default_team_id': self.env.user.sale_team_id.id,
            },
            'target': 'current',
        }

    def create_new_ticket(self):
        """
        Creates a new ticket.

        :return: A dictionary containing the details of the newly created ticket.
        :rtype: dict
        """
        self.env['agent.screen'].sudo().update_call_model('helpdesk.ticket')
        return {
            'name': 'Create New Ticket',
            'type': 'ir.actions.act_window',
            'res_model': 'helpdesk.ticket',
            'view_mode': 'form',
            'context': {
                'default_partner_id': self.partner_id.id,
                'default_user_id': self.env.user.id,
                'default_team_id': self.env.user.sale_team_id.id,
            },
            'target': 'current',
        }

    def create_new_subscription(self):
        """
        Creates a new subscription.

        :return: A dictionary containing the details of the created subscription.
        :rtype: dict
        """
        self.env['agent.screen'].sudo().update_call_model('sale.order')
        view_id = self.env.ref('sale_subscription.sale_subscription_primary_form_view').id
        return {
            'name': 'Create New Subscription',
            'type': 'ir.actions.act_window',
            'res_model': 'sale.order',
            'view_mode': 'form',
            'view_id': view_id,
            'context': {
                'default_partner_id': self.partner_id.id,
                'default_user_id': self.env.user.id,
                'default_team_id': self.env.user.sale_team_id.id,
                'default_is_subscription': True,
                'default_subscription_management': 'create',
            },
            'target': 'current',
        }

    def create_new_task(self):
        """
        Create a new task.

        This method updates the call model to 'project.task' in the 'agent.screen' environment,
        and returns a dictionary with the necessary values to create a new task.

        :return: A dictionary containing the values for creating a new task.
        :rtype: dict
        """
        self.env['agent.screen'].sudo().update_call_model('project.task')
        return {
            'name': 'Create New Task',
            'type': 'ir.actions.act_window',
            'res_model': 'project.task',
            'view_mode': 'form',
            'context': {
                'default_partner_id': self.partner_id.id,
                'default_user_ids': [(6, 0, [self.env.user.id])],
                'default_team_id': self.env.user.sale_team_id.id,
            },
            'target': 'current',
        }

    def create_new_call(self, model=False, record=False, mobile=False):
        """
        Creates a new call.

        :param model: (bool) Whether the call is a model or not. Defaults to False.
        :param record: (bool) Whether the call is a record or not. Defaults to False.
        :param mobile: (bool) Whether the call is a mobile call or not. Defaults to False.

        :return: None
        """
        message = {
            'action': 'call',
            'mobile': self.partner_id.mobile_trim if not mobile else mobile,
            'model': model,
            'record': record,
        }
        self.env['bus.bus'].sudo()._sendone(self.env.user.partner_id, 'ziwo/bus', message)

    def create_new_call_phone(self, model=False, record=False, phone=False):
        """
        Creates a new call on the phone.

        :param model: <bool> (optional) Whether to create a new model.
        :param record: <bool> (optional) Whether to record the call.
        :param phone: <bool> (optional) The phone number to call. If not provided, uses the partner's phone number.
        """
        message = {
            'action': 'call',
            'mobile': self.partner_id.phone_trim if not phone else phone,
            'model': model,
            'record': record,
        }
        self.env['bus.bus'].sudo()._sendone(self.env.user.partner_id, 'ziwo/bus', message)

    def update_call_model(self, model='agent.screen', record=False):
        """
        Updates the call model with the specified parameters.

        Args:
            model (str, optional): The name of the model to update. Defaults to 'agent.screen'.
            record (bool, optional): Whether to record the update or not. Defaults to False.

        Returns:
            None
        """
        message = {
            'action': 'update',
            'model': model,
            'record': record,
        }
        self.env['bus.bus'].sudo()._sendone(self.env.user.partner_id, 'ziwo/bus', message)
