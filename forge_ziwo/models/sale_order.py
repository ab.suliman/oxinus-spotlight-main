from odoo import models, fields, api

"""
Extends the sale.order model to include Ziwo-related fields and functionalities.
"""


class SaleOrder(models.Model):
    _inherit = "sale.order"

    partner_phone = fields.Char(related="partner_id.phone", string="Phone")
    partner_mobile = fields.Char(related="partner_id.mobile", string="Mobile")
    partner_parent = fields.Many2one(related="partner_id.parent_id", string="Company")

    ziwo_show_c2c_d2c = fields.Boolean(compute='_compute_ziwo_show_c2c_d2c')

    def _compute_ziwo_show_c2c_d2c(self):
        """ iterates over a collection of records. For each record, it checks if the record's name exists in a model called ziwo.active.form. If it does, it retrieves the model_id.model field from the matching records and assigns it to the ziwo_show_c2c_d2c field of the current record."""
        for record in self:
            record.ziwo_show_c2c_d2c = record._name in record.env['ziwo.active.form'].sudo().search([]).mapped(
                'model_id.model')

    # override on create to capture the two see if two context values exist (ziwo_model and ziwo_record)
    @api.model
    def create(self, vals):
        """overrides the create method from the superclass super(SaleOrder, self).
            Inside the method, it retrieves the values of ziwo_model and ziwo_record from the
            env.context dictionary. If both ziwo_model and ziwo_record are present,
            it searches for a record in the ziwo.history model with the matching ziwo_record ID,
            and updates the call model reference with the ID of the created sale order.
            Finally, it returns the result of the super.create method."""
        res = super(SaleOrder, self).create(vals)
        ziwo_model = self.env.context.get('ziwo_model')
        ziwo_record = self.env.context.get('ziwo_record')
        if ziwo_model and ziwo_record:
            self.env['ziwo.history'].sudo().search([('id', '=', ziwo_record)]).update_call_model_reference('sale.order',
                                                                                                           res.id)
        return res

    def action_add_note(self):
        return {
            "name": "Add Note",
            "type": "ir.actions.act_window",
            "view_mode": "form",
            "target": "new",
            "res_model": "add.note.wizard",
            "context": {"default_subscription_id": self.id},
        }

    def action_view_subscription(self):
        self.env['agent.screen'].sudo().update_call_model('sale.order', self.id)
        view_id = self.env.ref('sale_subscription.sale_subscription_primary_form_view').id
        return {
            "name": "View Subscription",
            "type": "ir.actions.act_window",
            'view_mode': 'form',
            'views': [(view_id, 'form')],
            "target": "current",
            "res_model": "sale.order",
            "res_id": self.id
        }

    def open_create_call_wizard(self):
        self.ensure_one()
        return {
            "type": "ir.actions.act_window",
            "res_model": "create.call.wizard",
            "view_mode": "form",
            "view_type": "form",
            "target": "new",
            "context": {
                "default_model": 'sale.order',
                "default_record": self.id,
            },
        }

    def create_new_call(self):
        """
        uses the sudo method to elevate the privileges and then calls the create_new_call method on an
        instance of the agent.screen class, passing in three arguments: the model name 'sale.order',
        the current object's id, and the mobile_trim attribute of the partner_id object.
        :return:
        """
        self.env['agent.screen'].sudo().create_new_call('sale.order', self.id, self.partner_id.mobile_trim)

    def create_new_call_phone(self):
        self.env['agent.screen'].sudo().create_new_call('sale.order', self.id, self.partner_id.phone_trim)
