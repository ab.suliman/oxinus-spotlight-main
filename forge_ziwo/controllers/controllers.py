# -*- coding: utf-8 -*-
from odoo import http
import base64

"""
Defines a controller to handle the route /forge_ziwo/ziwo_component/, which renders a template for the Ziwo web component.
"""


class ForgeZiwo(http.Controller):

    @http.route('/forge_ziwo/ziwo_component/', auth='public')
    def list(self, **kw):
        return http.request.render('forge_ziwo.ziwo_web_component', {})
