# -- coding: utf-8 --

from odoo import models, fields, api
import datetime

""" 
    This wizard allows users to add notes to various records such as opportunities (crm.lead), tickets (helpdesk.ticket),
    subscriptions (sale.order), tasks (project.task), and call history (ziwo.history). The add_note function posts the 
    note as a message to the relevant record.
"""


class AddNoteWizard(models.TransientModel):
    _name = 'add.note.wizard'
    _description = 'Add Note Wizard'

    opportunity_id = fields.Many2one(comodel_name="crm.lead", string="Opportunity")
    ticket_id = fields.Many2one(comodel_name="helpdesk.ticket", string="Ticket")
    subscription_id = fields.Many2one(comodel_name="sale.order", string="Subscription")
    task_id = fields.Many2one(comodel_name="project.task", string="Task")
    ziwo_history_id = fields.Many2one(comodel_name="ziwo.history", string="Call History")

    note = fields.Text(string="Note", required=1)

    def add_note(self):
        for record in self:
            if record.opportunity_id and record.note:
                record.opportunity_id.message_post(body=record.note)
            if record.ticket_id and record.note:
                record.ticket_id.message_post(body=record.note)
            if record.subscription_id and record.note:
                record.subscription_id.message_post(body=record.note)
            if record.ziwo_history_id and record.note:
                record.ziwo_history_id.message_post(body=record.note)
            if record.task_id and record.note:
                record.task_id.message_post(body=record.note)
