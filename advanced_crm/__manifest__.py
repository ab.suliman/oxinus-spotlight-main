# -*- coding: utf-8 -*-
{
    'name': "Advanced CRM",

    'summary': """
        This modules empowers the CRM module with advanced features and automations.""",

    'description': """
        This modules empowers the CRM module with advanced features and automations.
    """,

    'author': "Forge Solutions",
    'website': "https://www.forge-solutions.com",
    'license': 'Other proprietary',

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/16.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'crm', 'sales_team', 'sale', 'product', 'hr_holidays', 'approvals', 'project', 'documents',
                'multi_level_approval', 'analytic'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/view_logs_views.xml',
        'views/crm_lost_reason_views_inherit.xml',
        'views/crm_views_inherit.xml',
        'views/crm_team_views_inherit.xml',
        'views/sale_views_inherit.xml',
        'wizard/create_approval_request_wizard_views.xml',
        'wizard/create_task_wizard_views.xml',
        'wizard/disqualification_wizard_views.xml',
        'wizard/send_message_wizard.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
}
