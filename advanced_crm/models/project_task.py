# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ProjectTask(models.Model):
    _inherit = 'project.task'

    opportunity_id = fields.Many2one(comodel_name="crm.lead", string="Opportunity")
