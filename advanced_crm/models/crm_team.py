# -*- coding: utf-8 -*-

from odoo import models, fields, api
import json
from ast import literal_eval
from itertools import groupby
import re

"""Extends the crm.team model to manage sales team rotations and rules for opportunity
assignments."""


class CrmTeam(models.Model):
    _inherit = 'crm.team'

    salesperson_rotations_ids = fields.One2many('crm.team.rotations', 'team_id', string='Salesperson Rotations')
    salesperson_rotations_count = fields.Integer(string='Agents Rotations Count',
                                                 compute='_compute_salesperson_rotations_count')
    enable_create = fields.Boolean(string='Enable Create', default=True)
    enable_assign_to_me = fields.Boolean(string='Enable Assign To Me', default=True)

    @api.depends('salesperson_rotations_ids')
    def _compute_salesperson_rotations_count(self):
        for record in self:
            record.salesperson_rotations_count = len(record.salesperson_rotations_ids)

    def action_primary_channel_button(self):
        """
        Function comment for action_primary_channel_button method.

        :return: action - an instance of 'ir.actions.actions' with updated context and help
        :rtype: object
        """
        self.ensure_one()
        if self.use_opportunities:
            action = self.env['ir.actions.actions']._for_xml_id(
                'advanced_crm.salesteams_opportunity_with_domain')

            if self.env.user.has_group('base.group_system'):
                action = self.env['ir.actions.actions']._for_xml_id(
                    'advanced_crm.admin_opportunity_with_domain')

            if not action['context']:
                action['context'] = {}

            # action['context'] = {'default_team_id': self.id, 'default_type': 'opportunity', 'default_user_id': False,
            #                      'create': self.enable_create}

            context = action['context']
            if type(context) == str:
                context = context.replace('{', '').replace('}', '')
                key = "'create'"
                context = context + f', {key}: {self.enable_create}}}'
                context = f'{{{context}}}'
            else:
                context.update({
                    "create": self.enable_create
                })
            action['context'] = context
            rcontext = {
                'team': self,
            }
            action['help'] = self.env['ir.ui.view']._render_template(
                'crm.crm_action_helper', values=rcontext)
            return action
        return super(CrmTeam, self).action_primary_channel_button()


class CrmTeamRotations(models.Model):
    _name = 'crm.team.rotations'
    _description = 'CRM Team Rotations'

    team_id = fields.Many2one('crm.team', string='Team')
    user_id = fields.Many2one('res.users', string='User')
    sequence = fields.Integer(string='Sequence')
    employee_id = fields.Many2one(related='user_id.employee_id', string='Employee')
    is_absent = fields.Boolean(related='employee_id.is_absent', string='Is Absent')
    is_active = fields.Boolean(related='employee_id.active', string='Is Active')
    rule_domain = fields.Char(string="Business Rules", store=True)
