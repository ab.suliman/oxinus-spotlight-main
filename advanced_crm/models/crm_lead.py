# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError
from ast import literal_eval
from odoo.tools.safe_eval import safe_eval
from itertools import groupby

"""Extends the crm.lead model to add a variety of fields and methods for CRM automation.
It includes fields for tracking the opportunity's progress, the assignment to users, and customer state.
Implements methods for creating approval requests, task creation, stage transitions, and logging activities.
"""


class CrmLead(models.Model):
    _inherit = 'crm.lead'

    CUSTOMER_STATE_SELECTION = [
        ('hot', 'Hot'),
        ('cold', 'Cold')
    ]

    on_create_automation = fields.Boolean(string='On Create Automation', default=False)
    enable_rotation_assignment = fields.Boolean(related="stage_id.enable_rotation_assignment")
    assigned_date = fields.Datetime(string='Assigned Date', readonly=True)
    drop_date = fields.Datetime(string='Drop Date', readonly=True)
    remaining_time = fields.Float(string='Remaining Time')
    stage_notes = fields.Html(string='Stage Notes', related="stage_id.notes")
    current_stage_id = fields.Many2one('crm.stage', string='Current Stage')
    stage_code = fields.Char(related="stage_id.code")
    display_assign_button = fields.Boolean(compute="_compute_display_assign_button")
    assign_to_me_used = fields.Boolean(string='Assign to Me Used')
    # bdr_agent_id = fields.Many2one('res.users', string='BDR Agent')
    trade_license = fields.Binary(string='Trade License')
    last_user_id = fields.Many2one('res.users', string='Last Assigned User')
    created_by_salesperson = fields.Boolean(string='Created by Salesperson', compute="_compute_created_by_salesperson",
                                            store=True)

    customer_state = fields.Selection(selection=CUSTOMER_STATE_SELECTION, string='Customer State', tracking=True)

    def _search_viewed_by(self, operator, value):
        return [('id', operator, value)]

    viewed_by = fields.Many2one('res.users', string='Viewed By', compute="_assign_viewed_by",
                                search='_search_viewed_by')
    viewed_by_group_ids = fields.Many2many(related="viewed_by.groups_id", string='Viewed By Groups')
    viewed_by_admin = fields.Boolean(string='Viewed By Admin', compute="_assign_viewed_by")
    view_logs_ids = fields.One2many('view.logs', 'record_id', string='View Logs',
                                    domain="[('model_name', '=', 'crm.lead')]")

    date_deadline = fields.Date(string='Expected Closing', tracking=True)

    enable_create_task = fields.Boolean(related="stage_id.enable_create_task")
    requires_approval = fields.Boolean(related="stage_id.requires_approval")
    auto_create_task = fields.Boolean(related="stage_id.auto_create_task")
    auto_approval_request = fields.Boolean(related="stage_id.auto_approval_request")
    quotation_approved = fields.Boolean(related="stage_id.quotation_approved")

    latest_approval_request_id = fields.Many2one("approval.request", string="Latest Approval Request",
                                                 compute="_compute_latest_approval_request_id", store=True)
    latest_approval_request_status = fields.Selection(related="latest_approval_request_id.request_status",
                                                      string="Latest Approval Request Status", store=True)
    latest_order_id = fields.Many2one("sale.order", string="Latest Order", compute="_compute_latest_order_id",
                                      store=True)
    latest_order_status = fields.Selection(related="latest_order_id.state", string="Latest Order Status", store=True)

    previous_stage_id = fields.Many2one(comodel_name="crm.stage", string="Previous Stage")

    # ------------------------------------------  Smart Buttons -------------------------------------------------

    project_tasks_ids = fields.One2many("project.task", "opportunity_id", string="Tasks")
    project_tasks_count = fields.Integer(compute="_compute_project_tasks_count")

    @api.depends("project_tasks_ids")
    def _compute_project_tasks_count(self):
        """Computes the count of project tasks linked to this CRM lead."""
        for record in self:
            record["project_tasks_count"] = len(record.project_tasks_ids)

    def get_project_tasks(self):
        """
          Return an action to open the Project Tasks view for the current record.
        """
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'name': 'Project Tasks',
            'view_mode': 'tree,form',
            'res_model': 'project.task',
            'domain': [('opportunity_id', '=', self.id)],
            'context': {'create': False, 'default_opportunity_id': self.id,
                        'default_partner_id': self.partner_id.id}
        }

    approval_request_ids = fields.One2many("approval.request", "opportunity_id", string="Approvals", store=True)
    approval_requests_count = fields.Integer(compute="_compute_approval_requests_count")

    @api.depends("approval_request_ids", "approval_request_ids.request_status")
    def _compute_approval_requests_count(self):
        for record in self:
            record["approval_requests_count"] = len(record.approval_request_ids)

    def get_approval_requests(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'name': 'Approvals',
            'view_mode': 'tree,form',
            'res_model': 'approval.request',
            'domain': [('opportunity_id', '=', self.id)],
            'context': {'create': False, 'default_opportunity_id': self.id}
        }

    document_ids = fields.One2many("documents.document", "res_id", string="Documents")
    documents_count = fields.Integer(compute="_compute_documents_count")

    @api.depends("document_ids")
    def _compute_documents_count(self):
        for record in self:
            record["documents_count"] = len(record.document_ids)

    def get_documents(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'name': 'Documents',
            'view_mode': 'kanban,tree,form',
            'res_model': 'documents.document',
            'domain': [('res_model', '=', 'crm.lead'), ('res_id', '=', self.id)],
            'context': {'default_res_model': 'crm.lead', 'default_res_id': self.id, 'limit_folders_to_project': True}
        }

    # ------------------------------------------  Wizard Actions -------------------------------------------------

    def action_disqualified_wizard(self):
        """
        A function that handles the action of disqualifying a wizard.
        :return: A dictionary containing the details of the action window.
        :rtype: dict
        """
        # for record in self:
        #     return record.action_send_message_wizard('disqualified')
        view_id = self.env.ref('advanced_crm.disqualification_wizard_view_id').id
        return {
            'name': 'Disqualification',
            'type': 'ir.actions.act_window',
            'res_model': 'disqualification.wizard',
            'view_mode': 'form',
            'view_id': view_id,
            'target': 'new',
            'context': {'default_opportunity_id': self.id},
        }

    def action_send_message_wizard(self, additional_action):
        """
        Send a message wizard action.
        :param additional_action: Additional action to perform.
        :type additional_action: str
        :return: A dictionary representing the action to be performed.
        :rtype: dict
        """
        view_id = self.env.ref('advanced_crm.send_message_wizard_view_id').id
        return {
            'name': 'Send Message',
            'type': 'ir.actions.act_window',
            'res_model': 'send.message.wizard',
            'view_mode': 'form',
            'view_id': view_id,
            'target': 'new',
            'context': {'default_opportunity_id': self.id, 'additional_action': additional_action},
        }

    def action_create_task(self):
        """
        Create a new task and return an action window dictionary.
        :return: A dictionary containing the action window configuration.
        :rtype: dict
        """
        return {'type': 'ir.actions.act_window',
                'name': _('Create Task'),
                'res_model': 'create.task.wizard',
                'target': 'new',
                'view_id': self.env.ref('advanced_crm.view_create_task_wizard_form_advanced_crm').id,
                'view_mode': 'form',
                'context': {'default_opportunity_id': self.id, 'default_project_ids': self.stage_id.project_ids.ids,
                            'default_partner_id': self.partner_id.id}
                }

    def action_create_approval_request(self):
        """
        Create an approval request.

        :return: The created approval request as a dictionary.
        :rtype: dict
        """
        return {'type': 'ir.actions.act_window',
                'name': _('Create Approval Request'),
                'res_model': 'create.approval.request.wizard',
                'target': 'new',
                'view_id': self.env.ref(
                    'advanced_crm.view_create_approval_request_wizard_form').id,
                'view_mode': 'form',
                'context': {'default_opportunity_id': self.id}
                }

    # ------------------------------------------  Methods -------------------------------------------------

    @api.depends('create_uid', 'user_id')
    def _compute_created_by_salesperson(self):
        for record in self:
            record["created_by_salesperson"] = record.create_uid == record.user_id

    @api.depends()
    def _assign_viewed_by(self):
        for record in self:
            record["viewed_by"] = self.env.user.id
            record["viewed_by_admin"] = self.env.user.has_group('sales_team.group_sale_manager')
            record._create_view_log()
            # record._check_readonly()

    @api.depends("approval_request_ids")
    def _compute_latest_approval_request_id(self):
        for record in self:
            record.latest_approval_request_id = record.approval_request_ids[
                -1] if record.approval_request_ids else False

    @api.depends("order_ids")
    def _compute_latest_order_id(self):
        for record in self:
            record.latest_order_id = record.order_ids[
                -1] if record.order_ids else False

    def _log_note(self, message):
        for record in self:
            # if record.user_id:
            #     message = '@{} \n\n'.format(record.user_id.name)  + message
            record_id = record._origin.id
            if not record_id:
                record.id

            self.env['mail.message'].create({
                'res_id': record_id,
                'model': 'crm.lead',
                'body': message,
                'message_type': 'notification',
                'subtype_id': self.env.ref('mail.mt_note').id,
            })

    def _schedule_activity(self, message, activity_type_id):
        for record in self:
            record_id = record._origin.id
            if not record_id:
                record.id

            self.env['mail.activity'].create({
                'activity_type_id': activity_type_id.id,
                'date_deadline': fields.Date.today(),
                'summary': 'Notification',
                'note': message,
                'res_id': record_id,
                'res_model_id': self.env.ref('crm.model_crm_lead').id,
                'user_id': self.env.user.id,
            })

    def _check_next_stage_automation(self):
        """
        Check if the next stage can be automated for each record.
        This function iterates over each record and checks if the current stage and on_create_automation flag are set. If not, the function returns.
        If the user_id is not set and the current stage has the wait_for_assigned flag set, the function returns.
        If the current stage has a required_fields_domain and auto_next_stage flag set, the function checks if the record's filtered domain matches the required_fields_domain.
        If the filtered domain does not match, the function raises a UserError with a message indicating that approval/fields are required to proceed to the next stage.
        If the filtered domain matches, the function iterates over each next stage in the current stage's next_stage_ids and performs the following actions:
        - If the next stage has a rule_domain set, the function checks if the record's filtered domain matches the rule_domain.
            - If the filtered domain matches, the function updates the record's stage_id, current_stage_id, and team_id (if applicable) to the values of the next stage.
            - The function then calls the _onchange_stage_id method.
        - If the next stage does not have a rule_domain set, the function updates the record's stage_id, current_stage_id, and team_id (if applicable) to the values of the next stage.
            - The function then calls the _onchange_stage_id method.
        This function does not have any parameters and does not return anything.
        """
        for record in self:
            if not record.current_stage_id or not record.on_create_automation:
                return

            if not record.user_id and record.current_stage_id.wait_for_assigned:
                return

            if record.current_stage_id.required_fields_domain and record.current_stage_id.auto_next_stage:
                # and record.current_stage_id.next_stage_ids:

                if not record.filtered_domain(literal_eval(record.current_stage_id.required_fields_domain)):
                    # TODO: Need to fix this
                    # if record.stage_id != record.current_stage_id and record.stage_id not in [
                    #     self.env.ref('advanced_crm.stage_lead_pool'),
                    #     self.env.ref('advanced_crm.stage_no_answer'),
                    #     record.current_stage_id.drop_stage_id,
                    # ]:
                    if record.stage_id != record.current_stage_id and record.stage_id not in [
                        record.current_stage_id.drop_stage_id,
                    ]:
                        raise UserError(
                            'Approval/Fields are required to proceed to the next stage. Check the stage notes.')
                else:
                    for next_stage in record.current_stage_id.next_stage_ids:
                        if next_stage.rule_domain:
                            if record.filtered_domain(literal_eval(next_stage.rule_domain)):
                                record.write({
                                    "stage_id": next_stage.next_stage_id.id,
                                    "current_stage_id": next_stage.next_stage_id.id,
                                })
                                if next_stage.next_stage_id.team_id:
                                    record.write({
                                        "team_id": next_stage.next_stage_id.team_id.id,
                                    })
                                record._onchange_stage_id()
                        else:
                            record.write({
                                "stage_id": next_stage.next_stage_id.id,
                                "current_stage_id": next_stage.next_stage_id.id,
                            })
                            if next_stage.next_stage_id.team_id:
                                record.write({
                                    "team_id": next_stage.next_stage_id.team_id.id,
                                })
                            record._onchange_stage_id()

    def _clear_field_value(self, field_type):
        """
        Clears the value of a field based on its type.
        Args:
            field_type (str): The type of the field.
        Returns:
            int or list or dict or bool: The cleared value of the field based on its type.
                - If `field_type` is 'integer', 'float', or 'monetary', returns 0.
                - If `field_type` is 'one2many' or 'many2many', returns [(6, 0, [])].
                - If `field_type` is 'json', returns {}.
                - If `field_type` is any other value, returns False.
        """
        if field_type == 'integer' or field_type == 'float' or field_type == 'monetary':
            return 0
        elif field_type == 'one2many' or field_type == 'many2many':
            return [(6, 0, [])]
        elif field_type == 'json':
            return {}
        else:
            return False

    def _check_clear_fields(self):
        """
        Check and clear fields in the current record based on the clear fields defined in the current stage.
        """
        for record in self:
            for clear_field in record.stage_id.clear_field_ids:
                record[clear_field.name] = self._clear_field_value(clear_field.ttype)

    def _populate_field_value(self, populate_field):
        evaluation_type = populate_field.evaluation_type
        if evaluation_type == 'equation':
            return safe_eval(populate_field.value)
        elif populate_field.field_id.ttype in ['many2one', 'integer']:
            try:
                return int(populate_field.value)
            except Exception:
                pass
        else:
            return populate_field.value

    def _check_populate_fields(self):
        for record in self:
            for populate_field in record.stage_id.populate_field_ids:
                record[populate_field.field_id.name] = self._populate_field_value(populate_field)

    def _trigger_check_rotation(self):
        leads = self.env["crm.lead"].search([('enable_rotation_assignment', '=', True)])
        for lead in leads:
            lead._check_rotation_assignment()

    def _check_rotation_assignment(self):
        """
        Checks the rotation assignment for each record in the current object.
        This function iterates through each record in the current object and checks if the rotation
        assignment is enabled for the stage and if there are salesperson rotations available for the team.
        If both conditions are met, it sorts the salesperson rotations based on the sequence and
        performs the rotation assignment.

        Parameters:
            self (object): The current object.
        Returns:
            None
        """
        for record in self:
            if record.stage_id.enable_rotation_assignment and record.team_id.salesperson_rotations_ids:
                sorted_salesperson = sorted(record.team_id.salesperson_rotations_ids, key=lambda r: r.sequence)
                for salesperson in sorted_salesperson:
                    if not salesperson.is_absent:
                        if salesperson.rule_domain:
                            if record.filtered_domain(literal_eval(salesperson.rule_domain)):
                                if record.stage_id.user_field_id:
                                    record[record.stage_id.user_field_id.name] = salesperson.user_id.id
                                else:
                                    record.user_id = salesperson.user_id.id
                                if len(sorted_salesperson) > 1:
                                    salesperson.sequence = sorted_salesperson[-1].sequence + 1
                                    break
                        else:
                            if record.stage_id.user_field_id:
                                record[record.stage_id.user_field_id.name] = salesperson.user_id.id
                            else:
                                record.user_id = salesperson.user_id.id
                            if len(sorted_salesperson) > 1:
                                salesperson.sequence = sorted_salesperson[-1].sequence + 1
                                break

    # @api.onchange('stage_id')
    def _onchange_stage_id(self):
        """
        Automatically performs a series of actions when the stage ID of a record changes.
        This function is triggered when the stage ID of the record is updated.

        Parameters:
            self (Recordset): The current recordset.

        Returns:
            None
        """
        for record in self:
            # record._check_next_stage_automation()
            record._assign_current_stage()
            record._check_rotation_assignment()
            record._check_clear_fields()
            record._check_populate_fields()

            if record.stage_id.action_type == 'log_note' and record.stage_id.notes:
                # call action to log note
                self._log_note(record.stage_id.notes)
            if record.stage_id.action_type == 'schedule_activity':
                # call action to schedule activity for user_id
                self._schedule_activity(record.stage_id.notes, record.stage_id.activity_type_id)

            record._assign_remaining_time()
            record._assign_stage_team()

            if record.stage_id.auto_create_task:
                self.env['create.task.wizard'].with_context(
                    {'default_opportunity_id': self.id, 'default_project_ids': self.stage_id.project_ids.ids,
                     'default_partner_id': self.partner_id.id}).create({}).create_task()

            if record.stage_id.auto_approval_request:
                for approval_condition in record.stage_id.approval_condition_ids:
                    if not approval_condition.rule_domain or record.filtered_domain(
                            literal_eval(approval_condition.rule_domain)):
                        self.env['create.approval.request.wizard'].with_context(
                            {'default_opportunity_id': record.id,
                             'default_approval_type_id': approval_condition.approval_type_id.id}
                        ).create({}).create_approval_request()

    def _assign_stage_team(self):
        """
        Assigns the stage team to the record.

        This function iterates over the records in the current instance of the class.
        For each record, it checks if the stage has a team assigned to it and if the
        team assigned to the record is different from the team assigned to the stage.
        If the conditions are met, it updates the team assigned to the record with
        the team assigned to the stage.

        Parameters:
        - None

        Returns:
        - None
        """
        for record in self:
            if record.stage_id.team_id and record.team_id != record.stage_id.team_id:
                record.team_id = record.stage_id.team_id.id

    @api.onchange('user_id')
    def _update_assigned_date(self):
        """
        Updates the assigned date when the user ID is changed.

        :param self: The current recordset.
        :return: None
        """
        for record in self:
            if record.user_id:
                record["assigned_date"] = fields.Datetime.now()

    def _assign_remaining_time(self):
        """
        Assigns the remaining time for each record based on the allocated hours.

        This function iterates over each record in the current object and checks if it has
        allocated hours in the stage. If allocated hours exist, it iterates over each
        allocated hour and performs the following actions:
        - If a rule domain is specified, it evaluates the domain filter against the record's
        filtered domain. If the domain filter matches, it updates the remaining time for
        the record by adding the allocated hour's duration. If the 'accumulate' flag is
        set to True, the duration is added to the existing remaining time, otherwise,
        the new remaining time is set to the duration replaces the existing remaining time.
        - If a schedule activity flag is set, it schedules an activity with the specified
        message and activity type.

        If no allocated hours are found for a record, the remaining time is set to 0.

        Parameters:
        - None

        Return Type:
        - None
        """
        for record in self:
            if record.stage_id.allocated_hours_ids:  # and not record.created_by_salesperson
                for allocated_hours in record.stage_id.allocated_hours_ids:
                    if allocated_hours.rule_domain:
                        if record.filtered_domain(literal_eval(allocated_hours.rule_domain)):
                            if allocated_hours.accumulate:
                                record["remaining_time"] += allocated_hours.duration
                            else:
                                record["remaining_time"] = allocated_hours.duration

                            if allocated_hours.schedule_activity:
                                record._schedule_activity(allocated_hours.activity_message,
                                                          allocated_hours.activity_type_id)
                    else:
                        if allocated_hours.accumulate:
                            record["remaining_time"] += allocated_hours.duration
                        else:
                            record["remaining_time"] = allocated_hours.duration

                        if allocated_hours.schedule_activity:
                            record._schedule_activity(allocated_hours.activity_message,
                                                      allocated_hours.activity_type_id)
            else:
                record["remaining_time"] = 0

    def _update_remaining_time(self):
        """
        Update the remaining time for CRM leads based on the configured interval of the cron job.

        This function retrieves all CRM leads from the database and calculates the value to subtract
        from their remaining time based on the interval type and number of the cron job.
        It then iterates over each lead and checks if it meets the stage domain criteria.
        If it does, the function subtracts the calculated value from the lead's remaining time.
        If the remaining time becomes zero or negative, and the lead has a drop stage and allocated
        hours, the function updates the lead's stage, user, and remaining time accordingly.

        Parameters:
        - None

        Return:
        - None
        """
        leads = self.env["crm.lead"].search([])
        cron = self.env.ref("advanced_crm.ir_cron_update_crm_lead_remaining_time")
        value_to_subtract = 0
        if cron.interval_type == 'minutes':
            value_to_subtract = cron.interval_number / 60
        if cron.interval_type == 'hours':
            value_to_subtract = cron.interval_number
        if cron.interval_type == 'days':
            value_to_subtract = cron.interval_number * 24
        for record in leads:
            stage_domain = literal_eval(record.stage_id.drop_stage_domain) if record.stage_id.drop_stage_domain else []
            if record.filtered_domain(stage_domain):
                if record.user_id:  # and not record.created_by_salesperson
                    if record.remaining_time > 0:
                        if record.remaining_time - value_to_subtract <= 0:
                            if record.stage_id.drop_stage_id and record.stage_id.allocated_hours_ids:
                                record.write({
                                    "stage_id": record.stage_id.drop_stage_id.id,
                                    "user_id": False,
                                    "remaining_time": 0
                                })
                        else:
                            record.remaining_time = record.remaining_time - value_to_subtract
                    else:
                        if record.stage_id.drop_stage_id and record.stage_id.allocated_hours_ids:
                            record.write({
                                "stage_id": record.stage_id.drop_stage_id.id,
                                "user_id": False,
                                "remaining_time": 0
                            })

    def move_to_qualified(self):
        for record in self:
            return record.action_send_message_wizard('qualified')

    def move_to_no_answer(self):
        """
        Moves the current record to the 'No Answer' stage.

        :param self: The current record.
        :return: None
        """
        for record in self:
            if self.env.ref('advanced_crm.stage_no_answer'):
                record["stage_id"] = self.env.ref('advanced_crm.stage_no_answer').id
                record["current_stage_id"] = self.env.ref('advanced_crm.stage_no_answer').id

    def _assign_current_stage(self):
        """
        Assigns the current stage to each record in the current object.

        This function iterates over each record in the current object and checks if a stage_id
        is assigned. If a stage_id is found, it updates the current_stage_id field of the record
        with the stage_id value.

        Parameters:
            None

        Returns:
            None
        """
        for record in self:
            if record.stage_id:
                record.write({
                    "current_stage_id": record.stage_id.id
                })

    def action_close_deal(self):
        for record in self:
            return record.action_send_message_wizard('won')

    def action_assign_to_me(self):
        """
        Assigns the lead to the current user.

        Raises:
            UserError: If the current user was already assigned to this lead and cannot assign it to
            themselves again.

        Returns:
            None
        """
        for record in self:
            if self.env.user == record.last_user_id and record.stage_id.prevent_last_user_assigned:
                raise UserError('You were already assigned to this lead and cannot assign it to yourself again.')

            record.write({
                'user_id': self.env.user.id,
                'assign_to_me_used': True
            })

    @api.depends('stage_id', 'stage_id.code', 'current_stage_id', 'current_stage_id.required_fields_domain', 'user_id',
                 'team_id', 'team_id.enable_assign_to_me')
    def _compute_display_assign_button(self):
        """
        Compute the value of the 'display_assign_button' field for each record.

        This method is triggered whenever the values of the fields 'stage_id', 'stage_id.code',
        'current_stage_id', 'current_stage_id.required_fields_domain', 'user_id', 'team_id',
        and 'team_id.enable_assign_to_me' are modified.

        Parameters:
            self (object): The current recordset.

        Returns:
            None
        """
        for record in self:
            if record.current_stage_id.required_fields_domain:
                filled_required_fields = record.filtered_domain(
                    literal_eval(record.current_stage_id.required_fields_domain))
            else:
                filled_required_fields = True
            # if record.stage_id.code == 'LPL' and filled_required_fields and (not record.user_id) and record.team_id.enable_assign_to_me:
            if (not record.user_id) and record.team_id.enable_assign_to_me:
                record["display_assign_button"] = True
            else:
                record["display_assign_button"] = False

    def _create_view_log(self):
        """
        Create a view log for each record in the current object.

        :return: None
        """
        for record in self:
            record.view_logs_ids = [(0, 0, {
                'user_id': self.env.user.id,
                'model_id': self.env.ref('crm.model_crm_lead').id,
                'record_id': self.viewed_by.id,
                'date': fields.Datetime.now()
            })]

    @api.depends('user_id', 'type', 'assign_to_me_used')
    def _compute_team_id(self):
        """
         Compute the team_id based on the user_id, type, and assign_to_me_used fields.
         If the assign_to_me_used field is True, no further computation is needed.
         Otherwise, the team_id is computed by calling the parent class's _compute_team_id method.
         :return: None
         """
        for record in self:
            if record.assign_to_me_used:
                return
            else:
                return super()._compute_team_id()

    @api.onchange('')
    def _assign_last_user(self):
        """
        Assigns the last user ID to the 'last_user_id' field if the 'user_id' field is empty.

        :param self: The current recordset.
        :return: None
        """
        for record in self:
            if not record.user_id:
                record["last_user_id"] = record.user_id.id
