from odoo import api, fields, models


class ApprovalRequest(models.Model):
    _inherit = "approval.request"

    opportunity_id = fields.Many2one(comodel_name="crm.lead", string="Related Opportunity")
    opportunity_stage_id = fields.Many2one(comodel_name="crm.stage", string="Related Stage")

