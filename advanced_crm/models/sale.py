# -*- coding: utf-8 -*-

from odoo import models, fields, api


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    quotation_approved = fields.Boolean(string="Quotation Approved", compute="_get_quotation_approved")

    @api.depends()
    def _get_quotation_approved(self):
        """
        Calculate the quotation approval status for each record.

        If the record has an associated opportunity, the quotation approval status is set to
        the same value as the opportunity's quotation approval status. Otherwise, it is set
        to True.
        """
        for record in self:
            if record.opportunity_id:
                record.quotation_approved = record.opportunity_id.quotation_approved
            else:
                record.quotation_approved = True

    # def _update_opportunity_stage(self):
    #     for record in self:
    #         if record.opportunity_id and record.state == 'sent':
    #             quotation_sent_stage = self.env.ref("advanced_crm.stage_quotation_sent")
    #             record.opportunity_id.write({
    #                 "stage_id": quotation_sent_stage.id
    #             })
    #         if record.opportunity_id and record.state in ['sale', 'done']:
    #             quotation_confirmed_stage = self.env.ref("advanced_crm.stage_offer_accepted")
    #             record.opportunity_id.write({
    #                 "stage_id": quotation_confirmed_stage.id
    #             })
