# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ViewLogs(models.Model):
    _name = 'view.logs'
    _description = 'View Logs'

    model_id = fields.Many2one('ir.model', string='Model')
    model_name = fields.Char(string='Model Name', related="model_id.model")
    record_id = fields.Many2oneReference(string="Record ID", model_field="model_name", store=True)
    document_reference = fields.Char(string="Related Document",
                                     compute="_compute_document_reference")

    user_id = fields.Many2one('res.users', string='User')
    date = fields.Datetime(string='Date')

    @api.depends('model_name', 'record_id')
    def _compute_document_reference(self):
        """
        Compute the document reference based on the model name and record id.

        Args:
            self (object): The current record being processed.

        Returns:
            None
        """
        for rec in self:
            # Check if both the model name and record id are present
            if rec.model_name and rec.record_id:
                # If both are present, concatenate them with a comma and assign to document_reference
                rec.document_reference = "%s,%s" % (rec.model_name, rec.record_id)
            else:
                # If either the model name or record id is missing, assign an empty string to document_reference
                rec.document_reference = ""
