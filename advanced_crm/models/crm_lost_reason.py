# -*- coding: utf-8 -*-

from odoo import models, fields, api


class CrmLostReason(models.Model):
    _inherit = 'crm.lost.reason'

    email_template_ids = fields.Many2many("mail.template", "template_lost_reason_rel", column1="reason_id",
                                          column2="template_id", string="Email Templates",
                                          domain=[("model", "=", "crm.lead")])
