# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.tools.safe_eval import safe_eval
from odoo.exceptions import ValidationError

"""Extends the crm.stage model to include additional configurations for CRM stages,
such as auto-progression to the next stage, activities to be scheduled at
each stage, and rules for dropping to another stage if certain conditions are met."""


class CRMStage(models.Model):
    _inherit = 'crm.stage'

    code = fields.Char()
    notes = fields.Html(string='Notes', store=True)
    action_type = fields.Selection([
        ('none', 'None'),
        ('log_note', 'Log Note'),
        ('schedule_activity', 'Schedule Activity')
    ], default='none', string='Action Type', store=True)
    activity_type_id = fields.Many2one(comodel_name="mail.activity.type", string="Activity Type", store=True,
                                       default=lambda self: self.env.ref("mail.mail_activity_data_todo"))
    drop_stage_id = fields.Many2one('crm.stage', string='Drop Stage')
    next_stage_ids = fields.One2many('crm.stage.next.stage.automation', 'stage_id', string="Next Stage")

    auto_next_stage = fields.Boolean(default=False, string='Auto Next Stage')
    drop_stage_domain = fields.Char(string="Domain", store=True)
    allocated_hours_ids = fields.One2many('crm.stage.allocated.hours', 'stage_id', string='Allocated Hours')
    populate_field_ids = fields.One2many('crm.stage.populate.field', 'stage_id', string='Populate Field(s)')
    clear_field_ids = fields.Many2many('ir.model.fields', string='Clear Fields',
                                       domain=[('model', '=', 'crm.lead')])
    required_fields_domain = fields.Char(string="Required Fields", store=True)
    wait_for_assigned = fields.Boolean(string="Wait For Assigned", store=True)
    enable_rotation_assignment = fields.Boolean(string="Enable Rotation Assignment", store=True)
    user_field_id = fields.Many2one('ir.model.fields', string='User Field',
                                    domain=[('model', '=', 'crm.lead'), ('relation', '=', 'res.users')])
    requires_approval = fields.Boolean(string="Requires Approval")
    enable_create_task = fields.Boolean(string="Enable Create Task")
    approval_type_ids = fields.Many2many("approval.category", "approval_stage_rel", column1="stage_id",
                                         column2="approval_type_id", compute="_compute_approval_types", store=True,
                                         string="Approval Types")
    auto_create_task = fields.Boolean(string="Auto Create Task", store=True)
    auto_approval_request = fields.Boolean(string="Auto Approval Request", store=True)
    project_ids = fields.Many2many("project.project", "stage_project_rel", column1="stage_id", column2="project_id",
                                   string="Default Project(s)")
    document_tag_ids = fields.Many2many("documents.tag", "tag_stage_rel", column1="stage_id", column2="tag_id",
                                        string="Default Document Tags", store=True)
    prevent_last_user_assigned = fields.Boolean(string="Prevent Last User Assigned")
    approval_condition_ids = fields.One2many('crm.stage.approval.condition', 'stage_id', string='Approval Conditions')
    quotation_approved = fields.Boolean(string="Quotation Approved", default=False, store=True)

    @api.depends('approval_condition_ids')
    def _compute_approval_types(self):
        """
        Calculate the approval types based on the approval condition IDs.

        This method is triggered whenever the approval_condition_ids field is modified.

        :return: None
        """
        for record in self:
            record["approval_type_ids"] = record.approval_condition_ids.mapped('approval_type_id').ids

    @api.onchange('auto_create_task', 'auto_approval_request')
    def _enable_features(self):
        """
        Enable or disable certain features based on the values of auto_create_task and
        auto_approval_request.

        :param self: The current recordset.
        :return: None
        """
        for record in self:
            if record.auto_create_task:
                record.enable_create_task = record.auto_create_task
            if record.auto_approval_request:
                record.requires_approval = record.auto_approval_request

    @api.onchange('auto_next_stage', 'drop_stage_id')
    def _clear_dependency_fields(self):
        """
        Clears the dependency fields based on the values of the 'auto_next_stage'
        and 'drop_stage_id' fields.

        :return: None
        """
        for record in self:
            if not record.auto_next_stage:
                record.write({
                    "required_fields_domain": False,
                    "next_stage_ids": [(6, 0, [])]
                })

            if not record.drop_stage_id:
                record.write({
                    "allocated_hours_ids": [(6, 0, [])]
                })


class CRMStageAllocatedDaysHours(models.Model):
    _name = 'crm.stage.allocated.hours'
    _description = 'CRM Stage Allocated Hours'

    stage_id = fields.Many2one('crm.stage', string='Stage', ondelete="cascade")
    # field_id = fields.Many2one('ir.model.fields', string='Field', required=True, domain=[('model', '=', 'crm.lead')],
    #                            ondelete="cascade")
    # field_value = fields.Char(string='Value')
    rule_domain = fields.Char(string="Business Rules", store=True)
    duration = fields.Float(string='Duration (hours)')
    accumulate = fields.Boolean(string='Accumulate Hours', default=False)
    schedule_activity = fields.Boolean(string="Schedule Activity", default=False)
    activity_message = fields.Html(string="Activity Message", default="Provide an update")
    activity_type_id = fields.Many2one(comodel_name="mail.activity.type", string="Activity Type",
                                       default=lambda self: self.env.ref("mail.mail_activity_data_todo"))
    refresh_timer = fields.Boolean(string="Refresh Timer", default=False)


class CRMStageNextStageAutomation(models.Model):
    _name = 'crm.stage.next.stage.automation'
    _description = 'CRM Next Stage Automation'

    stage_id = fields.Many2one('crm.stage', string='Stage', ondelete="cascade")
    rule_domain = fields.Char(string="Business Rules", store=True)
    next_stage_id = fields.Many2one('crm.stage', string='Next Stage', ondelete="cascade")


class CRMStageApprovalCondition(models.Model):
    _name = 'crm.stage.approval.condition'
    _description = 'CRM Stage Approval Condition'

    stage_id = fields.Many2one('crm.stage', string='Stage', ondelete="cascade")
    approval_type_id = fields.Many2one('approval.category', string='Approval Type', ondelete="cascade",
                                       domain=[('apply_on_model', '=', 'crm.lead')])
    rule_domain = fields.Char(string="Business Rules", store=True)


class CRMStagePopulateFields(models.Model):
    _name = 'crm.stage.populate.field'
    _description = 'CRM Stage Populate Fields'

    stage_id = fields.Many2one('crm.stage', string='Stage', ondelete="cascade")

    field_id = fields.Many2one('ir.model.fields', string='Field', required=True, ondelete='cascade')
    value = fields.Text(required=True, help="Expression containing a value specification. \n"
                                            "When Formula type is selected, this field may be a Python expression "
                                            " that can use the same values as for the code field on the server action.\n"
                                            "If Value type is selected, the value will be used directly without evaluation.")
    evaluation_type = fields.Selection([
        ('value', 'Value'),
        ('reference', 'Reference'),
        ('equation', 'Python expression')
    ], 'Evaluation Type', default='value', required=True, change_default=True)
    resource_ref = fields.Reference(
        string='Record', selection='_selection_target_model',
        compute='_compute_resource_ref', inverse='_set_resource_ref')

    @api.model
    def _selection_target_model(self):
        return [(model.model, model.name) for model in self.env['ir.model'].sudo().search([])]

    @api.depends('field_id.relation', 'value', 'evaluation_type')
    def _compute_resource_ref(self):
        """
        Compute the resource reference for each record.

        :param self: The current object.
        :return: None
        """
        for line in self:
            if line.evaluation_type in ['reference', 'value'] and line.field_id and line.field_id.relation:
                value = line.value or ''
                try:
                    value = int(value)
                    if not self.env[line.field_id.relation].browse(value).exists():
                        record = list(self.env[line.field_id.relation]._search([], limit=1))
                        value = record[0] if record else 0
                except ValueError:
                    record = list(self.env[line.field_id.relation]._search([], limit=1))
                    value = record[0] if record else 0
                line.resource_ref = '%s,%s' % (line.field_id.relation, value)
            else:
                line.resource_ref = False

    @api.constrains('field_id', 'evaluation_type')
    def _raise_many2many_error(self):
        """
        This function is a constraint that raises a validation error if any record in the
        current set has a field_id of type 'many2many' and an evaluation_type of 'reference'.

        :param self: The current recordset.
        :return: None
        :raises: ValidationError if any record in the current set has a field_id of type
        'many2many' and an evaluation_type of 'reference'.
        """
        if self.filtered(lambda line: line.field_id.ttype == 'many2many' and line.evaluation_type == 'reference'):
            raise ValidationError(_('many2many fields cannot be evaluated by reference'))

    @api.onchange('resource_ref')
    def _set_resource_ref(self):
        """
        Set the value of the 'value' field for each record in the current recordset based on
        the value of the 'resource_ref' field.

        :return: None
        """
        for line in self.filtered(lambda line: line.evaluation_type == 'reference'):
            if line.resource_ref:
                line.value = str(line.resource_ref.id)

    def eval_value(self, eval_context=None):
        """
        Evaluate the value of each line in the object and return a dictionary with the results.

        Args:
            eval_context (dict, optional): The evaluation context to use for evaluating the expressions.
             Defaults to None.

        Returns:
            dict: A dictionary mapping each line's ID to its evaluated value.
        """
        result = {}
        for line in self:
            expr = line.value
            if line.evaluation_type == 'equation':
                expr = safe_eval(line.value, eval_context)
            elif line.field_id.ttype in ['many2one', 'integer']:
                try:
                    expr = int(line.value)
                except Exception:
                    pass
            result[line.id] = expr
        return result
