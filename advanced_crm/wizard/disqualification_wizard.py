# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError


class DisqualificationWizard(models.TransientModel):
    _name = 'disqualification.wizard'
    _description = 'Disqualification Wizard'

    opportunity_id = fields.Many2one('crm.lead', string='Opportunity', required=True)
    disqualified_reason_id = fields.Many2one('crm.lost.reason', string='Disqualification Reason', required=True)
    email_template_id = fields.Many2one('mail.template', string='Email Template')
    email_template = fields.Html(related="email_template_id.body_html")

    @api.onchange('disqualified_reason_id')
    def onchange_disqualified_reason_id(self):
        for record in self:
            record.email_template_id = False
            if record.disqualified_reason_id.email_template_ids:
                record.email_template_id = record.disqualified_reason_id.email_template_ids[0].id

            return {
                "domain": {"email_template_id": [('id', 'in', record.disqualified_reason_id.email_template_ids.ids)]}}

    def send_email(self):
        if not self.email_template_id:
            raise UserError('Please select an email template to send.')

        partner_id = self.opportunity_id.partner_id
        if not partner_id.email:
            raise UserError(_("Cannot send email: user %s has no email address.", partner_id.name))

        email_template_id = self.email_template_id
        # template = self.env['mail.template'].browse(email_template_id)
        email_template_id.send_mail(self.opportunity_id.id, force_send=True)

        self.opportunity_id.lost_reason_id = self.disqualified_reason_id.id
        self.opportunity_id.user_id = False
        self.opportunity_id.stage_id = self.env.ref('advanced_crm.stage_lead_pool').id

        return {'type': 'ir.actions.act_window_close'}
