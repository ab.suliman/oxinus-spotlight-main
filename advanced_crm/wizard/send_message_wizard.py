# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError


class SendMessageWizard(models.TransientModel):
    _name = 'send.message.wizard'
    _description = 'Send Message Wizard'

    opportunity_id = fields.Many2one('crm.lead', string='Opportunity', required=True)
    description = fields.Html(string="Description", required=True)

    def send_message(self):
        for record in self:
            if record.opportunity_id and record.description:
                record.opportunity_id._log_note(message=record.description)
                additional_action = self.env.context.get('additional_action', False)
                if additional_action == 'won':
                    self.mark_opportunity_as_won()
                if additional_action == 'qualified':
                    self.mark_opportunity_as_qualified()

    def mark_opportunity_as_won(self):
        for record in self:
            if record.opportunity_id:
                record.opportunity_id.action_set_won()

    def mark_opportunity_as_qualified(self):
        for record in self:
            if record.opportunity_id:
                record.opportunity_id.stage_id = self.env.ref('advanced_crm.stage_qualified').id
                record.opportunity_id.current_stage_id = self.env.ref('advanced_crm.stage_qualified').id
