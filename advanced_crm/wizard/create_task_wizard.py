# -- coding: utf-8 --

from odoo import models, fields, api
import datetime


class CreateTaskWizard(models.TransientModel):
    _name = 'create.task.wizard'
    _description = 'Create Task Wizard'

    project_ids = fields.Many2many("project.project", "wiz_project_rel", column1="wizard_id", column2="project_id",
                                   string="Project(s)", required=1)
    opportunity_id = fields.Many2one(comodel_name="crm.lead", string="Opportunity")
    partner_id = fields.Many2one(comodel_name="res.partner", string="Customer")

    def create_task(self):
        for record in self:
            for project_id in record.project_ids:
                self.env["project.task"].create({
                    "project_id": project_id.id,
                    "name": '[' + record.opportunity_id.stage_id.name + '] ' + record.opportunity_id.name,
                    "opportunity_id": record.opportunity_id.id,
                    "partner_id": record.partner_id.id
                });
