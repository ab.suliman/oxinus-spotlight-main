# -- coding: utf-8 --

from odoo import models, fields, api
from odoo.exceptions import UserError
import datetime


class CreateApprovalRequestWizard(models.TransientModel):
    _name = 'create.approval.request.wizard'
    _description = 'Create Approval Request Wizard'

    approval_type_id = fields.Many2one(comodel_name="approval.category", string="Approval Type", required=1)
    opportunity_id = fields.Many2one(comodel_name="crm.lead", string="Opportunity")
    stage_approval_type_ids = fields.Many2many(related="opportunity_id.stage_id.approval_type_ids")

    def create_approval_request(self):
        for record in self:
            if record.approval_type_id:
                similar_approval_requests = record.opportunity_id.approval_request_ids.filtered(
                    lambda lar: lar.category_id == record.approval_type_id and lar.request_status in ['new', 'pending'])
                if similar_approval_requests:
                    raise UserError('Similar approval request was already raised. '
                                    'If you want to create a new one, cancel the existing request.')

                self.env["approval.request"].create({
                    "request_owner_id": self.env.user.id,
                    "category_id": record.approval_type_id.id,
                    "related_record_id": record.opportunity_id.id,
                    "company_id": self.env.company.id
                })
