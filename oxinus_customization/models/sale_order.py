from odoo import models, fields, api

""" 
Extends the sale.order model with custom fields and logic.
Overrides the action_confirm method to assign an account manager if not already assigned.
Includes related KYC and SPOC fields from the associated CRM lead.
Adds custom logic to handle attachments separately from mail thread attachments.
"""


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    # oxinus_crm_lead_id = fields.Many2one('crm.lead', string='CRM Lead', readonly=True)

    # @api.onchange('opportunity_id')
    # def _inherit_onchange_opportunity_id(self):
    #     if self.opportunity_id:
    #         self.oxinus_crm_lead_id = self.opportunity_id.id
    #     else:
    #         self.oxinus_crm_lead_id = False

    # override action_confirm
    def action_confirm(self):
        """
        is a method called action_confirm in a class. It performs the following steps:

        It checks if the account_manager_id attribute is empty and the team_id attribute is not empty.
        If the conditions are met, it searches for a default account manager in the crm.team model based on the team_id.
        If a default account manager is found, it assigns the account_manager_id attribute to the default account manager.
        It schedules an activity for the assigned account manager.
        It calls the action_confirm method of the superclass SaleOrder and stores the result in the res variable.
        It returns the result.
        """
        if not self.account_manager_id and self.team_id:
            account_manager_default = self.env['crm.team'].sudo().search([('id', '=', self.team_id.id)],
                                                                         limit=1).lead_account_manager.id
            if account_manager_default:
                self.account_manager_id = account_manager_default
                self.activity_schedule(
                    'mail.mail_activity_data_todo',
                    res_id=self.id,
                    user_id=self.account_manager_id.id,
                    summary=('You have been assigned to %s as Account Manager.') % self.name
                )
        res = super(SaleOrder, self).action_confirm()
        return res

    def _get_attachments_search_domain(self):
        self.ensure_one()
        return [('res_id', '=', self.id), ('res_model', '=', 'sale.order')]

    def _compute_attachment_ids(self):
        """
        Compute the attachment IDs for each record.

        This function searches for attachments related to each record and updates the 'attachment_ids' field accordingly.
        It first retrieves the attachment IDs using the '_get_attachments_search_domain' method of each record.
        It then retrieves the attachment IDs from the 'message_ids.attachment_ids' field of each record using the 'mapped' method.
        Finally, it computes the difference between the two sets of attachment IDs and assigns the result to the 'attachment_ids' field.

        Parameters:
            self (Recordset): The current recordset.

        Returns:
            None
        """
        for record in self:
            attachment_ids = self.env['ir.attachment'].search(record._get_attachments_search_domain()).ids
            message_attachment_ids = record.mapped('message_ids.attachment_ids').ids  # from mail_thread
            record.attachment_ids = [(6, 0, list(set(attachment_ids) - set(message_attachment_ids)))]

    attachment_ids = fields.One2many('ir.attachment', compute='_compute_attachment_ids', string="Main Attachments",
                                     help="Attachments that don't come from a message.")

    account_manager_id = fields.Many2one(comodel_name="res.users", string="Account Manager", store=True, tracking=True)

    # KYC fields:
    kyc_company_brand_name = fields.Char(string='Company Brand Names', related='opportunity_id.kyc_company_brand_name',
                                         readonly=True)
    kyc_number_of_branches_to_onboard = fields.Integer(string='Branches to be Onboarded',
                                                       related='opportunity_id.kyc_number_of_branches_to_onboard',
                                                       readonly=True)
    kyc_number_of_branches_to_add = fields.Integer(string='Branches to be Added Later',
                                                   related='opportunity_id.kyc_number_of_branches_to_add',
                                                   readonly=True)
    kyc_has_delivery = fields.Boolean(string='Delivery', related='opportunity_id.kyc_has_delivery', readonly=True)
    kyc_delivery_self = fields.Boolean(string='Self', related='opportunity_id.kyc_delivery_self', readonly=True)
    kyc_delivery_aggregator = fields.Boolean(string='Aggregator', related='opportunity_id.kyc_delivery_aggregator',
                                             readonly=True)
    # kyc_self_or_aggregator = fields.Selection(string='Self or Aggregator', related='opportunity_id.kyc_self_or_aggregator', readonly=True)
    kyc_aggregator_name = fields.Many2many('res.partner', string='Aggregators',
                                           related='opportunity_id.kyc_aggregator_name',
                                           domain=[('kyc_is_aggregator', '=', True)], readonly=True)
    kyc_be_working = fields.Selection(string='WiFi/Wired', related='opportunity_id.kyc_be_working', readonly=True)
    kyc_be_using_kds = fields.Selection(string='KDS', related='opportunity_id.kyc_be_using_kds', readonly=True)
    kyc_using_own_devices = fields.Boolean(string='Using Own Devices', related='opportunity_id.kyc_using_own_devices',
                                           readonly=True)
    kyc_own_devices = fields.One2many('kyc.device', 'crm_lead_id', string='Devices',
                                      related='opportunity_id.kyc_own_devices', readonly=True)

    # SPOC Details:
    spoc_name = fields.Many2one('res.partner', string='Name', related='opportunity_id.spoc_name', readonly=True)
    spoc_phone = fields.Char(string='Phone', related='spoc_name.phone', readonly=True)
    spoc_email = fields.Char(string='Email', related='spoc_name.email', readonly=True)
    spoc_language = fields.Char(string='Preferred Language', related='opportunity_id.spoc_language', readonly=True)
    spoc_street = fields.Char(string='Address', related='spoc_name.street', readonly=True)
    spoc_street2 = fields.Char(string='Address', related='spoc_name.street2', readonly=True)
    spoc_city = fields.Char(string='City', related='spoc_name.city', readonly=True)
    spoc_state_id = fields.Many2one('res.country.state', string='State', related='spoc_name.state_id', readonly=True)
    spoc_zip = fields.Char(string='Zip', related='spoc_name.zip', readonly=True)
    spoc_country_id = fields.Many2one('res.country', string='Country', related='spoc_name.country_id', readonly=True)
