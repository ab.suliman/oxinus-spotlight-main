from odoo import models, fields, api

"""Overrides the create method to set the user ID when a new ticket is created.
"""


class HelpdeskTicket(models.Model):
    _inherit = 'helpdesk.ticket'

    @api.model
    def create(self, vals):
        if 'email_cc' not in vals:
            vals['user_id'] = self.env.user.id
        return super(HelpdeskTicket, self).create(vals)
