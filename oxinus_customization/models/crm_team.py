from odoo import models, fields, api, _

"""Enhances the crm.team model with a field to assign a lead account manager.
"""


class CrmTeam(models.Model):
    _inherit = 'crm.team'

    lead_account_manager = fields.Many2one('res.users', string='Lead Account Manager', tracking=True)
