from odoo import models, fields, api

""" Extends the crm.stage model to include a Boolean field to allow manual dropping of leads to a pool."""


class CrmStage(models.Model):
    _inherit = 'crm.stage'

    allow_manual_drop_to_pool = fields.Boolean(string='Allow Manual Drop')
