from odoo import models, fields, api, _

""" 
Extends the crm.lead model with additional KYC (Know Your Customer) fields, such as company brand names, number of 
branches, delivery options, and device details.
Defines fields for SPOC (Single Point of Contact) details, including name, phone, email, language, and address.
Includes logic for manual dropping to a pool, navigating to the sales team, and handling changes in delivery options.
"""


class CrmLead(models.Model):
    _inherit = 'crm.lead'

    # KYC fields:
    kyc_company_brand_name = fields.Char(string='Company Brand Names')
    kyc_number_of_branches_to_onboard = fields.Integer(string='Branches to be Onboarded')
    kyc_number_of_branches_to_add = fields.Integer(string='Branches to be Added Later')
    kyc_has_delivery = fields.Boolean(string='Delivery')
    kyc_delivery_self = fields.Boolean(string='Self')
    kyc_delivery_aggregator = fields.Boolean(string='Aggregator')
    # kyc_self_or_aggregator = fields.Selection([('self', 'Self'), ('aggregator', 'Aggregator'), ('self_and_aggregator', 'Self & Aggregator')], string='Self or Aggregator')
    kyc_aggregator_name = fields.Many2many('res.partner', string='Aggregators',
                                           domain=[('kyc_is_aggregator', '=', True)])
    kyc_be_working = fields.Selection([('wifi', 'WiFi'), ('wired', 'Wired')], string='WiFi/Wired')
    kyc_be_using_kds = fields.Selection([('printer', 'Printer'), ('screen', 'Screen')], string='KDS')
    kyc_using_own_devices = fields.Boolean(string='Using Own Devices')
    kyc_own_devices = fields.One2many('kyc.device', 'crm_lead_id', string='Devices')

    # SPOC Details:
    spoc_name = fields.Many2one('res.partner', string='Name', default=lambda self: self.partner_id)
    spoc_phone = fields.Char(string='Phone', related='spoc_name.phone')
    spoc_email = fields.Char(string='Email', related='spoc_name.email')
    spoc_language = fields.Char(string='Preferred Language')
    spoc_street = fields.Char(string='Address', related='spoc_name.street')
    spoc_street2 = fields.Char(string='Address', related='spoc_name.street2')
    spoc_city = fields.Char(string='City', related='spoc_name.city')
    spoc_state_id = fields.Many2one('res.country.state', string='State', related='spoc_name.state_id')
    spoc_zip = fields.Char(string='Zip', related='spoc_name.zip')
    spoc_country_id = fields.Many2one('res.country', string='Country', related='spoc_name.country_id')

    expected_locaitons = fields.Integer(string="Expected Locations", store=True)
    demo_required = fields.Boolean(string="Demo Required", store=True, default=True)
    demo_date = fields.Datetime(string="Demo Date", store=True)

    show_manual_drop_to_pool = fields.Boolean(string='Show Manual Drop to Pool', readonly=True,
                                              compute='_compute_show_manual_drop_to_pool')

    @api.depends('stage_id', 'stage_id.allow_manual_drop_to_pool', 'stage_id.drop_stage_id')
    def _compute_show_manual_drop_to_pool(self):
        for rec in self:
            rec.show_manual_drop_to_pool = False
            if rec.stage_id and rec.stage_id.allow_manual_drop_to_pool and rec.stage_id.drop_stage_id:
                rec.show_manual_drop_to_pool = True

    def manual_drop_to_pool(self):
        self.ensure_one()
        for record in self:
            record.drop_to_pool()
            # return record.navigate_to_team()

    def drop_to_pool(self):
        self.ensure_one()
        for record in self:
            current_stage = record.stage_id
            record.write({"user_id": False})
            record.write({'stage_id': current_stage.drop_stage_id.id})

    def navigate_to_team(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'name': _('Sales Teams'),
            'res_model': 'crm.team',
            'view_id': self.env.ref('advanced_crm.crm_team_view_kanban_dashboard_advanced_crm').id,
            'view_mode': 'kanban',
            'target': 'main',
        }

    @api.onchange('kyc_has_delivery', 'kyc_delivery_self', 'kyc_delivery_aggregator')
    def _onchange_kyc_has_delivery(self):
        for record in self:
            if not record.kyc_has_delivery:
                record.kyc_delivery_self = False
                record.kyc_delivery_aggregator = False
                record.kyc_aggregator_name = False
            elif not record.kyc_delivery_aggregator:
                record.kyc_aggregator_name = False
