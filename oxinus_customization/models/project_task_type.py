from odoo import models, fields, api

"""Adds a Boolean field to the project.task.type model to indicate whether a stage is considered complete."""


class ProjectTaskType(models.Model):
    _inherit = 'project.task.type'

    is_complete = fields.Boolean(string='Is Complete', default=False)
