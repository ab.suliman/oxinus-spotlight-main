# -*- coding: utf-8 -*-

from odoo import models, fields, api

"""Defines a new model for KYC devices with fields for device name, compatibility status, and a relation to CRM leads."""


class KycDevice(models.Model):
    _name = 'kyc.device'
    _description = 'KYC Device'

    name = fields.Char(string="Device", required=True)
    compatible = fields.Selection([('yes', 'Yes'), ('no', 'No'), ('maybe', 'Maybe')], string="Compatible",
                                  required=True)
    crm_lead_id = fields.Many2one(comodel_name="crm.lead", string="Opportunity")
