from odoo import models, fields, api

"""Extends the res.partner model with a Boolean field to indicate if a partner is an aggregator.
"""


class ResPartner(models.Model):
    _inherit = 'res.partner'

    kyc_is_aggregator = fields.Boolean(string='Is Aggregator')
