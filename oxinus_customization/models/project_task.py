from odoo import models, fields, api

"""  
Extends the project.task model with logic to copy messages and attachments from related sales orders upon task creation.
Includes related fields for KYC details and SPOC details from the associated sales order's opportunity.
Adds logic for stage change notifications and project manager notifications.
"""


class ProjectTask(models.Model):
    _inherit = 'project.task'

    @api.model
    def create(self, vals):
        record = super(ProjectTask, self).create(vals)
        if record.sale_order_id:
            for message in record.sale_order_id.message_ids:
                if message.attachment_ids:
                    record.message_post(body=message.body, attachment_ids=message.attachment_ids.ids)
            for attachment in record.sale_order_id.attachment_ids:
                attachment.copy(default={'res_id': record.id, 'res_model': 'project.task'})
        return record

    # @api.onchange('project_id')
    def _notify_project_manager(self):
        for record in self:
            if record.project_id.user_id:
                record.activity_schedule(
                    'mail.mail_activity_data_todo',
                    res_id=record.id,
                    user_id=record.project_id.user_id.id,
                    summary=('Assign %s') % record.name
                )

    @api.onchange('stage_id')
    def _onchange_stage_id(self):
        """
        Onchange method triggered when the 'stage_id' field is changed.

        This method is responsible for performing certain actions when the 'stage_id'
        field is changed. It iterates over the current recordset and checks if the
        'stage_id' is marked as complete, and if the 'sale_order_id' and
        'account_manager_id' fields are not empty. If all conditions are met, it
        schedules an activity on the 'sale_order_id' with the 'mail.mail_activity_data_todo'
        activity type. The activity is assigned to the 'account_manager_id' and has a
        summary that includes the name of the sale order.

        Parameters:
            self (RecordSet): The current recordset.

        Return:
            None
        """
        for record in self:
            if record.stage_id.is_complete and record.sale_order_id and record.sale_order_id.account_manager_id:
                record.sale_order_id.activity_schedule(
                    'mail.mail_activity_data_todo',
                    res_id=record.sale_order_id.id,
                    user_id=record.sale_order_id.account_manager_id.id,
                    summary=('Post Onboarding Follow up: %s') % record.sale_order_id.name
                )

    oxinus_opportunity_id = fields.Many2one('crm.lead', string='CRM Lead', related='sale_order_id.opportunity_id',
                                            readonly=True)

    # KYC fields:
    kyc_company_brand_name = fields.Char(string='Company Brand Names',
                                         related='sale_order_id.opportunity_id.kyc_company_brand_name', readonly=True)
    kyc_number_of_branches_to_onboard = fields.Integer(string='Branches to be Onboarded',
                                                       related='sale_order_id.opportunity_id.kyc_number_of_branches_to_onboard',
                                                       readonly=True)
    kyc_number_of_branches_to_add = fields.Integer(string='Branches to be Added Later',
                                                   related='sale_order_id.opportunity_id.kyc_number_of_branches_to_add',
                                                   readonly=True)
    kyc_has_delivery = fields.Boolean(string='Delivery', related='sale_order_id.opportunity_id.kyc_has_delivery',
                                      readonly=True)
    kyc_delivery_self = fields.Boolean(string='Self', related='sale_order_id.opportunity_id.kyc_delivery_self',
                                       readonly=True)
    kyc_delivery_aggregator = fields.Boolean(string='Aggregator',
                                             related='sale_order_id.opportunity_id.kyc_delivery_aggregator',
                                             readonly=True)
    # kyc_self_or_aggregator = fields.Selection(string='Self or Aggregator', related='sale_order_id.opportunity_id.kyc_self_or_aggregator', readonly=True)
    kyc_aggregator_name = fields.Many2many('res.partner', related='sale_order_id.opportunity_id.kyc_aggregator_name',
                                           domain=[('kyc_is_aggregator', '=', True)], readonly=True)
    kyc_be_working = fields.Selection(string='WiFi/Wired', related='sale_order_id.opportunity_id.kyc_be_working',
                                      readonly=True)
    kyc_be_using_kds = fields.Selection(string='KDS', related='sale_order_id.opportunity_id.kyc_be_using_kds',
                                        readonly=True)
    kyc_using_own_devices = fields.Boolean(string='Using Own Devices',
                                           related='sale_order_id.opportunity_id.kyc_using_own_devices', readonly=True)
    kyc_own_devices = fields.One2many('kyc.device', 'crm_lead_id', string='Devices',
                                      related='sale_order_id.opportunity_id.kyc_own_devices', readonly=True)

    # SPOC Details:
    spoc_name = fields.Many2one('res.partner', string='Name', related='sale_order_id.opportunity_id.spoc_name',
                                readonly=True)
    spoc_phone = fields.Char(string='Phone', related='spoc_name.phone', readonly=True)
    spoc_email = fields.Char(string='Email', related='spoc_name.email', readonly=True)
    spoc_language = fields.Char(string='Preferred Language', related='sale_order_id.opportunity_id.spoc_language',
                                readonly=True)
    spoc_street = fields.Char(string='Address', related='spoc_name.street', readonly=True)
    spoc_street2 = fields.Char(string='Address', related='spoc_name.street2', readonly=True)
    spoc_city = fields.Char(string='City', related='spoc_name.city', readonly=True)
    spoc_state_id = fields.Many2one('res.country.state', string='State', related='spoc_name.state_id', readonly=True)
    spoc_zip = fields.Char(string='Zip', related='spoc_name.zip', readonly=True)
    spoc_country_id = fields.Many2one('res.country', string='Country', related='spoc_name.country_id', readonly=True)
