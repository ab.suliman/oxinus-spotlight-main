# -*- coding: utf-8 -*-
{
    'name': "Oxinus Customizations",

    'summary': """This module contains Oxinus customizations""",

    'description': """This module contains Oxinus customizations""",

    'author': 'Forge Solutions',
    'website': 'https://www.forge-solutions.com',

    'category': 'Tools',
    'version': '1.03',
    'license': 'Other proprietary',

    'depends': ['base', 'mail', 'crm', 'sale', 'project', 'product', 'helpdesk', 'sale_crm', 'industry_fsm_sale', 'advanced_crm'],

    'data': [
        'security/ir.model.access.csv',
        'views/res_partner_views_inherit.xml',
        'views/crm_stage_views_inherit.xml',
        'views/crm_views_inherit.xml',
        'views/sale_order_views_inherit.xml',
        'views/project_task_inherit_views.xml',
        'views/project_task_type_inherit_views.xml',
        'views/crm_team_views_inherit.xml',
        'reports/sale_order_template.xml',
    ],

    # 'assets': {
    #     'web.assets_backend': [
    #         'oxinus_customization/static/src/js/hide_archive_button.js',
    #     ],
    # },
}