odoo.define('oxinus_customization.BasicView', function (require) {
    "use strict";

    var session = require('web.session');
    var BasicView = require('web.BasicView');
    BasicView.include({
        init: function (viewInfo, params) {
            var self = this;
            this._super.apply(this, arguments);
            var modelName = self.controllerParams.modelName;
            var model = modelName in ['crm.lead', 'sale.order', 'account.move'] ? 'True' : 'False';
            if (model) {
                self.controllerParams.archiveEnabled = 'False' in viewInfo.fields;
                // session.user_has_group('').then(function (has_group) {
                //     if (!has_group) {
                //         self.controllerParams.archiveEnabled = 'False' in viewInfo.fields;
                //     }
                // });                
            }
        },
    });
});