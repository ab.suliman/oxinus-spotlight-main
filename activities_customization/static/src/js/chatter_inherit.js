/** @odoo-module **/

import { registerPatch } from '@mail/model/model_core';
import { useService } from "@web/core/utils/hooks";

registerPatch({
    name: 'Chatter',
    recordMethods: {
        async onClickChatterOpenWhatsappWizard(event) {
            if (this.isTemporary) {
                const saved = await this.doSaveRecord();
                if (!saved) {
                    return;
                }
            }

            const action_content = {
                name: "Send Message",
                type: "ir.actions.act_window",
                view_mode: "form",
                target: "new",
                res_model: "whatsapp.message.wizard",
                extras: {
                    record_id: this.threadId,
                    model_name: this.threadModel,
                }
            };
            
            const create_action = await this.env.services.orm.call('forge.whatsapp.sudo.override', 'create_act_window', [action_content], {});
            console.log('create_action', create_action);

            this.env.services.action.doAction(create_action);
        },
    },
});