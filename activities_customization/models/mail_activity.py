from odoo import models, fields, api, _
from odoo.exceptions import UserError, AccessError


class MailActivity(models.Model):
    _inherit = 'mail.activity'

    # viewed_by_create_user = fields.Boolean(string='Viewed By Admin/Create User', readonly=True, compute='_compute_viewed_by_create_user')

    # @api.depends('res_id', 'res_model', 'user_id', 'viewed_by_create_user')
    # def _compute_viewed_by_create_user(self):
    #     for activity in self:
    #         activity.viewed_by_create_user = (activity.user_id == activity.create_uid) or self.env.user.has_group('base.group_system')

    def unlink(self):
        """
        Unlink method that deletes the current record.

        Raises:
            AccessError: If the activity is created by another user and the current user does not have system group access.

        Returns:
            bool: True if the record is successfully deleted, False otherwise.
        """
        for activity in self:
            # Check if the activity is created by another user and the current user does not have system group access
            if activity.create_uid and activity.create_uid != self.env.user and not self.env.user.has_group(
                    'base.group_system'):
                raise AccessError(_('You cannot cancel activities created by other users.'))

        return super().unlink()
