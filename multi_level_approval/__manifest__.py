# -*- coding: utf-8 -*-
{
    'name': "Multi-level Approvals",

    'summary': """
        Multi-level Approvals For All Models""",

    'description': """
        This module is used to enable multi-level approvals within the Approvals Module. 
        The multi-level approval can be applied to all models
    """,

    'author': "Forge Solutions",
    'website': "http://www.forge-solutions.com",
    'license': 'Other proprietary',

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Approvals',
    'version': '0.5',

    # any module necessary for this one to work correctly
    'depends': ['base', 'approvals', 'purchase', 'approvals_purchase_stock', 'stock'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/approval_matrix.xml',
        'views/approval_request_inherit.xml',
        'views/approver_views.xml',
        'views/approval_types_inherit.xml',
        'views/multi_approval_settings_inherit.xml',
        'views/purchase_views_inherit.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
