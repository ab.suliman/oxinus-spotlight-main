# -*- coding: utf-8 -*-

from odoo import models, fields, api
from ast import literal_eval

""" 
Extends the approval.approver model to include a manager approval flag.
Methods to handle the approval logic, like approving all, checking the next approver, creating approvers, etc.
"""


class custom_approver(models.Model):
    _inherit = 'approval.approver'

    manager_approval = fields.Boolean(string="Manager Approval", store=True, default=False)

    def approve_group(self, request, group):
        for approver in request.approver_ids:
            if group in approver.user_id.groups_id:
                approver.status = 'approved'

    def approve_all(self, approver_ids):
        for approver in approver_ids:
            approver.status = 'approved'

    def create_approver(self, record, approver_id, approvers):
        if approver_id:
            for index, approver in enumerate(approvers):
                if approver.user_id.id == approver_id:
                    return

            vals = {
                "user_id": approver_id,
                "request_id": record.id,
                "status": 'pending',
            }
            approver = self.env['approval.approver'].create(vals)
            approver._create_activity()

    def check_domain(self, document_record, order_domain, line_domain, line_field):
        allow = False
        if not order_domain or document_record.filtered_domain(literal_eval(order_domain)):
            if line_domain:
                for line in document_record[line_field.name]:
                    if line.filtered_domain(literal_eval(line_domain)):
                        allow = True
            else:
                allow = True
        else:
            if line_domain:
                for line in document_record[line_field.name]:
                    if line.filtered_domain(literal_eval(line_domain)):
                        allow = True

        return allow

    def create_for_user_or_group(self, record, approver):
        if approver.approver_group:
            for user in approver.approver_group.users:
                next_approver_id = user.id
                self.create_approver(record, next_approver_id, record.approver_ids)
        else:
            next_approver_id = approver.approver.id
            self.create_approver(record, next_approver_id, record.approver_ids)

    def check_for_next_approver(self):
        """
        Check for the next approver in the approval process.

        This function iterates over a collection of records and performs checks to determine if there is a
        next approver in the approval process. It considers various factors such as the approval type,
        the applied model, the document ID, and the current status of the record.

        Parameters:
            None

        Returns:
            None
        """
        for record in self:
            approval_type = record.request_id.category_id
            apply_on_model = record.request_id.related_model
            document_id = record.request_id.related_record_id
            document_record = record.env[apply_on_model].browse(document_id)
            approvers = approval_type.approval_matrix
            line_field = approval_type.apply_on_line_field

            if len(approvers) > 0:
                if record.status == 'approved':
                    if approval_type.final_approval_decides:
                        if record.user_id.id == approvers[-1].approver.id:
                            self.approve_all(record.request_id.approver_ids)

                    else:
                        if approval_type.hierarchical_approval:
                            if len(approvers) > len(record.request_id.approver_ids):
                                next_rule_domain = approvers[len(record.request_id.approver_ids)].rule_domain
                                next_line_rule_domain = approvers[len(record.request_id.approver_ids)].line_rule_domain
                                # if document_record.filtered_domain(literal_eval(next_rule_domain)):
                                if self.check_domain(document_record, next_rule_domain, next_line_rule_domain,
                                                     line_field):
                                    next_approver_id = record.user_id.employee_id.parent_id.user_id.id
                                    self.create_approver(record.request_id, next_approver_id,
                                                         record.request_id.approver_ids)
                        else:
                            next_index = -1
                            if record.manager_approval:
                                next_index = 0
                                if approval_type.condition_approval:
                                    for approver in approvers:
                                        if self.check_domain(document_record, approver.rule_domain,
                                                             approver.line_rule_domain, line_field):
                                            self.create_for_user_or_group(record.request_id, approver)
                            else:
                                for index, approver in enumerate(approvers):
                                    if not approver.approver_group:
                                        if approver['approver'].id == record.user_id.id:
                                            next_index = index + 1
                                            break
                                    else:
                                        if approver.approver_group in record.user_id.groups_id:
                                            self.approve_group(record.request_id, approver.approver_group)
                                            next_index = index + 1
                                            break

                            if next_index >= 0:
                                if next_index < len(approvers):
                                    next_rule_domain = approvers[next_index].rule_domain
                                    next_line_rule_domain = approvers[next_index].line_rule_domain
                                    if self.check_domain(document_record, next_rule_domain, next_line_rule_domain,
                                                         line_field):
                                        if not approval_type.condition_approval:
                                            self.create_for_user_or_group(record.request_id, approvers[next_index])
