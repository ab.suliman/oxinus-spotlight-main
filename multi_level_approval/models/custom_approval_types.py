# -*- coding: utf-8 -*-

from odoo import models, fields, api

""" 
Extends the approval.category model to include additional fields for configuring approval types, like sequential approval, condition approval, hierarchical approval, reset approval on update, etc.
Methods to manage how the approval process should behave based on these configurations.
"""


class custom_approval_types(models.Model):
    _inherit = 'approval.category'
    # _name = 'multi_level_approval.multi_level_approval'
    # _description = 'multi_level_approval.multi_level_approval'

    # approval_type = fields.Selection(selection_add=[("model", "Model Based")])
    apply_on_model = fields.Many2one(comodel_name="ir.model",
                                     string="Apply On Model")
    apply_on_line_field = fields.Many2one(comodel_name="ir.model.fields",
                                          string="Apply On Line Field")
    apply_on_line_model = fields.Char(related="apply_on_line_field.relation",
                                      string="Apply On Line Model Name")
    apply_on_line_model_name = fields.Char(related="apply_on_line_field.relation",
                                           string="Apply On Line Model Name")
    # apply_on_group = fields.Many2one(comodel_name="res.group",
    #                                  string="Apply On Group")
    sequential_approval = fields.Boolean(string="Sequential Approval?", default=True)
    condition_approval = fields.Boolean(string="Condition Approval?", default=False)
    hierarchical_approval = fields.Boolean(string="Hierarchical Approval?", default=False)
    final_approval_decides = fields.Boolean(string="Final Approval Decides?", default=False)
    reset_approval_on_update = fields.Boolean(string="Reset Approval On Update?", default=False)
    lock_approved_document = fields.Boolean(string="Lock Approved Document?", default=False)
    manager_approval_first = fields.Boolean(string="Manager Approval First", default=False)
    approval_matrix = fields.One2many("approval.matrix", "approval_type",
                                      string="Approval Matrix")

    operation_type_id = fields.Many2one(comodel_name="stock.picking.type",
                                        string="Default Operation Type",
                                        store=True)

    enable_transfer = fields.Boolean(string="Enable Transfers?",
                                     store=True)

    @api.onchange('sequential_approval', 'condition_approval')
    def reverse_checkbox(self):
        """
        Reverse the values of the 'sequential_approval' and 'condition_approval' fields.

        This function is triggered whenever either the 'sequential_approval' or
        'condition_approval' fields are modified. It checks the current value of
        'condition_approval' and sets 'sequential_approval' to False if 'condition_approval'
        is True. It also sets 'condition_approval' and 'final_approval_decides' to False
        if 'sequential_approval' is True.

        Parameters:
            self (object): The current instance of the class.

        Returns:
            None
        """
        if self.condition_approval:
            self.sequential_approval = False

        if self.sequential_approval:
            self.condition_approval = False
            self.final_approval_decides = False

    @api.onchange('hierarchical_approval')
    def reverse_manager_approval_first(self):
        if self.hierarchical_approval:
            self.manager_approval_first = False

    # @api.depends('value')
    # def _value_pc(self):
    #     for record in self:
    #         record.value2 = float(record.value) / 100
