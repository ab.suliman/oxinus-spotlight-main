# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from ast import literal_eval
from odoo.exceptions import UserError

""" 
Extends the approval.request model to include fields for related model, related record ID, document reference, 
purchase type, related internal transfer, and transfer enablement.
Methods to check and create approvers, manage approval status, and create internal transfers or RFQs
 based on the approvals.
"""


class custom_approval_request(models.Model):
    _inherit = 'approval.request'

    related_model = fields.Char(string="Related Model", compute="_compute_related_model", store=True)
    related_record_id = fields.Many2oneReference(string="Related Record ID", model_field="related_model", store=True)
    related_document = fields.Char(string="Related Document",
                                   compute="_compute_reference")
    purchase_type = fields.Selection([
        ('external', 'External Purchase'),
        ('internal', 'Internal Purchase')], string="Purchase Type", store=True)

    related_internal_transfer = fields.Many2one(comodel_name="stock.picking",
                                                string="Related Internal Transfer",
                                                store=True)

    enable_transfer = fields.Boolean(string="Enable Transfers?", related="category_id.enable_transfer")

    @api.depends('category_id')
    def _compute_related_model(self):
        """
        Compute the related model based on the category ID.

        This method is decorated with `@api.depends` to specify that it depends on the `category_id` field.
        It iterates over each record in the current object and checks if the `category_id` is set and if it has
        an `apply_on_model` field. If both conditions are met, it assigns the value of the `apply_on_model.model`
        to the `related_model` field. Otherwise, it assigns an empty string.

        Parameters:
            self (Recordset): The current recordset.

        Returns:
            None
        """
        for rec in self:
            if rec.category_id and rec.category_id.apply_on_model:
                rec.related_model = rec.category_id.apply_on_model.model
            else:
                rec.related_model = ""

    @api.depends('related_model', 'related_record_id')
    def _compute_reference(self):
        """
        Compute the value for the field "related_document" based on the values of the fields "related_model" and "related_record_id".

        :param self: The current recordset.
        :return: None
        """
        for rec in self:
            if rec.related_model and rec.related_record_id:
                rec.related_document = "%s,%s" % (rec.related_model, rec.related_record_id)
            else:
                rec.related_document = ""

    def create_approver(self, record, approver_id, manager=False):
        """
        Creates an approver for the given record.

        Args:
            record (Record): The record for which the approver is being created.
            approver_id (int): The ID of the user who will be the approver.
            manager (bool, optional): Indicates whether the approver is a manager. Defaults to False.

        Returns:
            None

        Side Effects:
            - Creates a new instance of `approval.approver` with the provided values.
            - Calls the `_create_activity` method on the created approver instance.
        """
        if approver_id:
            for approver in record.approver_ids:
                if approver.user_id.id == approver_id:
                    return

            vals = {
                "user_id": approver_id,
                "request_id": record.id,
                "status": 'pending',
                "manager_approval": manager
            }
            approver = self.env['approval.approver'].create(vals)
            approver._create_activity()

    def check_domain(self, document_record, order_domain, line_domain, line_field):
        """
        Check if the given domain matches the document record and line domain.

        :param document_record: The document record to check against.
        :type document_record: DocumentRecord

        :param order_domain: The domain to check against the document record.
        :type order_domain: str

        :param line_domain: The domain to check against each line in the document record.
        :type line_domain: str

        :param line_field: The field to use to get the lines from the document record.
        :type line_field: str

        :return: True if the domain matches any of the conditions, False otherwise.
        :rtype: bool
        """
        allow = False
        if not order_domain or document_record.filtered_domain(literal_eval(order_domain)):
            if line_domain:
                for line in document_record[line_field.name]:
                    if line.filtered_domain(literal_eval(line_domain)):
                        allow = True
            else:
                allow = True
        else:
            if line_domain:
                for line in document_record[line_field.name]:
                    if line.filtered_domain(literal_eval(line_domain)):
                        allow = True

        return allow

    def create_for_user_or_group(self, record, approver):
        if approver.approver_group:
            for user in approver.approver_group.users:
                next_approver_id = user.id
                self.create_approver(record, next_approver_id)
        else:
            next_approver_id = approver.approver.id
            self.create_approver(record, next_approver_id)

    def check_for_approvers(self):
        """
        Iterates over each record and checks for approvers based on the approval type.
        a method called check_for_approvers that iterates over a collection of records. It checks for approvers based
        on the approval type of each record. If certain conditions are met, it creates an approver for the record.
        The method also handles different scenarios for sequential and hierarchical approval processes.
        :return: None
        """
        for record in self:
            approval_type = record.category_id
            document_id = record.related_record_id
            if not record.related_model:
                raise UserError("Configure the related model in the Approval Type configuration.")
            document_record = record.env[record.related_model].browse(document_id)
            approvers = approval_type.approval_matrix
            line_field = approval_type.apply_on_line_field

            if approval_type.manager_approval_first:
                self.create_approver(record, record.request_owner_id.employee_id.parent_id.user_id.id, True);
            else:
                if len(approvers) > 0:
                    end_index = 0
                    add_first = False
                    for index, approver in enumerate(approvers):
                        next_rule_domain = approvers[index].rule_domain
                        next_line_rule_domain = approvers[index].line_rule_domain
                        # if not next_rule_domain or document_record.filtered_domain(literal_eval(next_rule_domain)):
                        if self.check_domain(document_record, next_rule_domain, next_line_rule_domain, line_field):
                            end_index = index
                            add_first = True
                            if approval_type.condition_approval:
                                self.create_for_user_or_group(record, approvers[index])
                                # if approvers[index].approver_group:
                                #     for user in approvers[index].approver_group.users:
                                #         next_approver_id = user.id
                                #         self.create_approver(record, next_approver_id)
                                # else:
                                #     next_approver_id = approvers[index].approver.id
                                #     self.create_approver(record, next_approver_id)

                    if approval_type.hierarchical_approval and add_first:
                        next_approver_id = record.request_owner_id.employee_id.parent_id.user_id.id
                        self.create_approver(record, next_approver_id)
                        manager_user = self.env['res.users'].browse(
                            record.request_owner_id.employee_id.parent_id.user_id.id)
                        if not approval_type.sequential_approval:
                            while manager_user.employee_id.parent_id:
                                current_approver = self.env['hr.employee'].browse(manager_user.employee_id.parent_id.id)
                                manager_user = self.env['res.users'].browse(current_approver.user_id.id)
                                self.create_approver(record, manager_user.id)
                    else:
                        if approval_type.sequential_approval and add_first:
                            self.create_for_user_or_group(record, approvers[0])
                            # if approvers[0].approver_group:
                            #     for user in approvers[0].approver_group.users:
                            #         next_approver_id = user.id
                            #         self.create_approver(record, next_approver_id)
                            # else:
                            #     next_approver_id = approvers[0].approver.id
                            #     self.create_approver(record, next_approver_id)
                        elif not approval_type.sequential_approval and not approval_type.condition_approval:
                            for i in range(0, end_index + 1):
                                self.create_for_user_or_group(record, approvers[i])
                                # if approvers[i].approver_group:
                                #     for user in approvers[i].approver_group.users:
                                #         next_approver_id = user.id
                                #         self.create_approver(record, next_approver_id)
                                # else:
                                #     next_approver_id = approvers[i].approver.id
                                #     self.create_approver(record, next_approver_id)

    def check_for_related_record(self):
        if not self.related_record_id:
            self['related_record_id'] = self.id
        self.check_for_approvers()

    def action_confirm(self):
        """
        Confirm the action.

        This function confirms the action by performing the following steps:
        1. Check if the number of approver IDs is less than the minimum required.
           - If the condition is true, raise a UserError with an error message.
        2. Check for related records.
        3. If the `requirer_document` field is set to 'required' and no attachment number is provided,
           raise a UserError with an error message.
        4. Filter the mapped 'approver_ids' to get only the ones with a status of 'new'.
        5. If there are any approvers, create an activity, set their status to 'pending', and write the changes.
        6. Update the 'date_confirmed' field with the current datetime.

        This function does not have any parameters or return any values.
        """
        # if len(self.approver_ids) < self.approval_minimum:
        #     raise UserError(_("You have to add at least %s approvers to confirm your request.", self.approval_minimum))
        self.check_for_related_record()
        if self.requirer_document == 'required' and not self.attachment_number:
            raise UserError(_("You have to attach at lease one document."))
        approvers = self.mapped('approver_ids').filtered(lambda approver: approver.status == 'new')
        if approvers:
            approvers._create_activity()
            approvers.write({'status': 'pending'})
        self.write({'date_confirmed': fields.Datetime.now()})

    def create_internal_transfer(self):
        """
        a method called create_internal_transfer which returns a dictionary. The dictionary contains various key-value
        pairs representing different properties of an action window. These properties include the type of action window,
        its name, the model it is associated with, the target for opening the window, the view ID to be used, the view
        mode, and a context dictionary with a default value for the approval_request_id key.
        """
        return {'type': 'ir.actions.act_window',
                'name': _('Create Transfer'),
                'res_model': 'create.transfer.wizard',
                'target': 'new',
                'view_id': self.env.ref('multi_level_approval.view_create_transfer_wizard_form').id,
                'view_mode': 'form',
                'context': {'default_approval_request_id': self.id}
                }

    def create_rfq_custom(self):
        return {'type': 'ir.actions.act_window',
                'name': _('Create RFQ'),
                'res_model': 'create.rfq.wizard',
                'target': 'new',
                'view_id': self.env.ref('multi_level_approval.view_create_rfq_wizard_form_custom_fields').id,
                'view_mode': 'form',
                'context': {'default_request_ids': self.ids}
                }

    def show_inventory_report(self):
        return {'type': 'ir.actions.act_window',
                'name': _('Inventory Report'),
                'res_model': 'stock.quant',
                'target': 'new',
                'view_id': self.env.ref('multi_level_approval.view_stock_quant_tree_custom_view').id,
                'view_mode': 'tree',
                'context': {'search_default_internal_loc': 1, 'search_default_productgroup': 1,
                            'search_default_locationgroup': 1,
                            },
                'domain': [('product_id', 'in', self.product_line_ids.mapped('product_id').ids)]
                }


class ApprovalProductLine(models.Model):
    _inherit = 'approval.product.line'

    approval_type = fields.Many2one(related="approval_request_id.category_id")
    request_status = fields.Selection(related="approval_request_id.request_status")

    def create_rfq_custom(self):
        """
        Create RFQ and return an action window dictionary.

        """
        return {'type': 'ir.actions.act_window',
                'name': _('Create RFQ'),
                'res_model': 'create.rfq.wizard',
                'target': 'new',
                'view_id': self.env.ref('multi_level_approval.view_create_rfq_wizard_form_custom_fields').id,
                'view_mode': 'form',
                'context': {'default_approval_product_lines': self.ids}
                }
