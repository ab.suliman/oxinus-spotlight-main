# -*- coding: utf-8 -*-

from odoo import models, fields, api

""" 
Defines an approval matrix model with fields like approver, approver group, lock record option, 
condition for the approval, approval type, and related model details. This model is used to configure the criteria and
approvers for each step of the multi-level approval process.
"""


class approval_matrix(models.Model):
    _name = 'approval.matrix'
    _description = 'Approval Matrix'

    approver = fields.Many2one(comodel_name="res.users",
                               string="Approver")
    approver_group = fields.Many2one(comodel_name="res.groups",
                                     string="Approver Group")
    # minimum_amount = fields.Float(string="Minimum Order Amount", default=0)
    lock_record = fields.Boolean(string="Lock Record?", default=False)
    rule_domain = fields.Char(string="Condition", help="Condition for the approval to be applied")
    line_rule_domain = fields.Char(string="Line Condition", help="Condition for the approval to be applied")
    approval_type = fields.Many2one(comodel_name="approval.category",
                                    string="Approval Type")
    apply_on_model_name = fields.Char(string="Apply On Model", related="approval_type.apply_on_model.model")
    apply_on_line_model_name = fields.Char(string="Apply On Model", related="approval_type.apply_on_line_model")
    hierarchical_approval = fields.Boolean(string="Hierarchical Approval",
                                           related="approval_type.hierarchical_approval")
