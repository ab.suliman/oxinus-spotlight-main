# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError
from ast import literal_eval

""" 
Extends the purchase.order model to integrate the multi-level approval process into the purchase workflow.
Fields to track approval requests, status, and whether the purchase order requires approval.
Methods to send approval requests, confirm purchase orders, and create manager activities.
"""


class purchase_approval_inherit(models.Model):
    _inherit = 'purchase.order'

    requires_approval = fields.Boolean(string="Requires Approval?",
                                       readonly=1,
                                       default=False,
                                       compute="_check_apply_approval")

    approval_request = fields.Many2one(comodel_name="approval.request",
                                       string="Approval Request")

    approval_type = fields.Many2one(comodel_name="approval.category",
                                    string="Approval Request")

    approval_request_status = fields.Selection(related="approval_request.request_status",
                                               string="Approval Status")

    submitted_to_manager = fields.Boolean(string="Submitted to Manager", store=True, default=False)

    @api.depends('amount_total')
    def _check_apply_approval(self):
        """
    a method _check_apply_approval that is decorated with @api.depends('amount_total'). This means that the method will
    be triggered whenever the amount_total field is changed.
    Inside the method, there is a loop that iterates over each record. It checks if a certain configuration parameter
    purchase.apply_approval is set to "True". If it is, it sets the requires_approval field of the current record to False.
    Then, it checks if the approval_request_status field of the record is not equal to 'approved'. If it is not,
    it retrieves the default approval type ID from another configuration parameter purchase.default_approval_type.
    If the default approval type ID is found, it retrieves the corresponding approval.category record and gets the list
    f approvers from the approval_matrix field. It then iterates over each approver and checks if their rule_domain is
    defined. If it is, it evaluates the domain expression using literal_eval and applies it to the record using
    filtered_domain. If the record matches the domain, it sets the approval_request_status and requires_approval
    fields of the record to empty string and True respectively, and breaks out of the loop.
    If the rule_domain is not defined for an approver, it sets the approval_request_status and requires_approval
    fields of the record to empty string and True respectively, and breaks out of the loop.
    If the purchase.apply_approval configuration parameter is not set to "True", it sets the requires_approval
    field of the record to False, and the approval_request_status field to 'approved'.
        """
        for record in self:
            if self.env['ir.config_parameter'].sudo().get_param('purchase.apply_approval') == "True":
                record.requires_approval = False
                if record.approval_request_status != 'approved':
                    approval_type_id = int(
                        self.env['ir.config_parameter'].sudo().get_param('purchase.default_approval_type'))
                    if approval_type_id:
                        approval_type = self.env['approval.category'].browse(approval_type_id)
                        if approval_type:
                            approvers = approval_type.approval_matrix
                            for approver in approvers:
                                if approver.rule_domain:
                                    if record.filtered_domain(literal_eval(approver.rule_domain)):
                                        record.approval_request_status = ''
                                        record.requires_approval = True
                                        break
                                else:
                                    record.approval_request_status = ''
                                    record.requires_approval = True
                                    break
            else:
                record.requires_approval = False
                record.approval_request_status = 'approved'

    def send_approval_request(self):
        """
        Sends an approval request for each record in the current object.
        The function retrieves the default approval type from the 'ir.config_parameter' model
        and creates an approval request if the approval type is found.

        :return: None
        """
        for record in self:
            approval_type_id = int(self.env['ir.config_parameter'].sudo().get_param('purchase.default_approval_type'))
            if approval_type_id:
                approval_type = self.env['approval.category'].browse(approval_type_id)
                if approval_type:
                    if record.requires_approval and not record.approval_request:
                        vals = {
                            "name": "Approval: " + record.name,
                            "request_owner_id": record.user_id.id,
                            "category_id": approval_type.id,
                            "related_record_id": record.id,
                        }
                        approval_request = self.env['approval.request'].create(vals)
                        record.approval_request = approval_request.id
                    # else:
                    #     record.approval_request_status = 'approved'

    def button_confirm(self):
        if self.requires_approval and not self.approval_request_status:
            raise UserError("Approval Request not approved yet")
        else:
            super(purchase_approval_inherit, self).button_confirm()

    def create_manager_activity(self):
        for record in self:
            self.env['mail.activity'].create({
                'res_model_id': self.env.ref('purchase.model_purchase_order').id,
                'activity_type_id': 4,
                'res_id': record.id,
                'user_id': record.user_id.employee_parent_id.user_id.id,
                'summary': "Approval Required",
            })
            record.submitted_to_manager = True
