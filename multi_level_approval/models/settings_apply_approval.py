# -*- coding: utf-8 -*-

from odoo import models, fields, api

""" 
A settings model to configure the multi-level approval feature within Odoo's settings.
Fields to enable multi-level approvals and set the default approval type for purchases.
"""


class settings_apply_approval(models.TransientModel):
    _inherit = 'res.config.settings'

    purchase_apply_approval = fields.Boolean(string="Multi-Level Approvals",
                                             default=False, config_parameter="purchase.apply_approval")
    purchase_default_approval_type = fields.Many2one(comodel_name="approval.category",
                                                     string="Default Approval Type",
                                                     config_parameter="purchase.default_approval_type")
