# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from ast import literal_eval
import datetime

""" 
A wizard for creating internal stock transfers based on approval requests.
Includes fields for operation type, destination location, and product lines.
Method create_internal_transfer to generate the stock picking (transfer) records.
"""


class create_transfer_wizard(models.TransientModel):
    _name = 'create.transfer.wizard'
    _description = 'Create Transfer Wizard'

    approval_request_id = fields.Many2one(comodel_name="approval.request", string="Related Request")

    operation_type = fields.Many2one(comodel_name="stock.picking.type",
                                     string="Operation type",
                                     )

    # source_location = fields.Many2one(comodel_name="stock.location",
    #                                   string="Source Location",
    #                                   )

    destination_location = fields.Many2one(comodel_name="stock.location",
                                           string="Destination Location",
                                           )

    product_line_ids = fields.One2many("create.transfer.wizard.product.line",
                                       "wizard_id",
                                       string="Product Lines",
                                       )

    @api.onchange('approval_request_id')
    def add_product_lines(self):
        """
        This function is an onchange method triggered when the "approval_request_id" field is changed.
        It adds product lines to the current record based on the approval request's product line ids.

        :return: None
        """
        for record in self:
            # if record["product_line_ids"]:
            #     record["product_line_ids"] = (5)
            lines = []
            for product_line in record.approval_request_id.product_line_ids:
                if product_line.warehouse_id:
                    line_id = {
                        'description': product_line.description,
                        'product_id': product_line.product_id.id,
                        'quantity': product_line.quantity,
                        'product_uom_id': product_line.product_uom_id.id,
                        'warehouse_id': product_line.warehouse_id.id,
                        'wizard_id': record.id
                    }
                    lines.append((0, 0, line_id))
            record.product_line_ids = lines

    @api.onchange('operation_type')
    def assign_source_and_dest_locations(self):
        for record in self:
            # record["source_location"] = record.operation_type.default_location_src_id.id
            record["destination_location"] = record.operation_type.default_location_dest_id.id

    def create_internal_transfer(self):
        """
        Creates internal transfers for the current record.
        This function iterates over each record and creates internal transfers based on the product lines and warehouses associated with the record.

        :return: None
        """
        for record in self:
            warehouses = record.product_line_ids.mapped('warehouse_id')
            for warehouse in warehouses:
                val = {
                    'name': record.operation_type.sequence_id.next_by_id(),
                    'picking_type_id': record.operation_type.id,
                    'location_id': warehouse.lot_stock_id.id,
                    'location_dest_id': record.destination_location.id,
                    'move_type': 'one',
                    'company_id': record.approval_request_id.company_id.id
                }
                internal_transfer = self.env['stock.picking'].create(val)

                if record.approval_request_id.boq_id:
                    internal_transfer.boq_id = record.approval_request_id.boq_id.id

                record.approval_request_id.related_internal_transfer = internal_transfer.id
                # transfer_lines = []
                for product_line in record.product_line_ids:
                    if product_line.warehouse_id.id == warehouse.id:
                        val = {
                            'company_id': record.approval_request_id.company_id.id,
                            'date': datetime.datetime.now(),
                            'location_id': product_line.source_location.id,
                            'location_dest_id': record.destination_location.id,
                            'name': product_line.description,
                            'product_id': product_line.product_id.id,
                            'product_uom_qty': product_line.quantity,
                            'product_uom': product_line.product_uom_id.id,
                            'picking_id': internal_transfer.id
                        }
                        # transfer_lines.append((0, 0, val))
                        self.env['stock.move'].create(val)


class create_transfer_wizard_product_line(models.TransientModel):
    _name = 'create.transfer.wizard.product.line'
    _description = 'Create Transfer Wizard Product Line'

    wizard_id = fields.Many2one(comodel_name="create.transfer.wizard", string="CTW", ondelete="cascade")
    product_id = fields.Many2one(comodel_name="product.product", string="Product")
    description = fields.Char(string="Description")
    quantity = fields.Float(string="Quantity")
    product_uom_id = fields.Many2one(comodel_name="uom.uom", string="UoM")
    warehouse_id = fields.Many2one(comodel_name="stock.warehouse", string="Warehouse")
    source_location = fields.Many2one(related="warehouse_id.lot_stock_id", string="Source Location")
    destination_location = fields.Many2one(related="wizard_id.destination_location", string="Destination Location")
    operation_type = fields.Many2one(related="wizard_id.operation_type", string="Operation Type")
