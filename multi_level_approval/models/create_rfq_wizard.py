# -- coding: utf-8 --

from odoo import models, fields, api
import datetime

""" 
A wizard to create RFQs (Request for Quotations) based on approval requests.
Includes fields for the partner, approval requests, lines for quotation, and the RFQ reference.
Methods to add lines to the order and create a new RFQ are defined.
"""


class AddToQuotationWizard(models.TransientModel):
    _name = 'create.rfq.wizard'
    _description = 'Create RFQ Wizard'

    # action_model = fields.Selection([('purchase.order', 'RFQ'), ('sale.order', 'Quotation')])
    action_type = fields.Selection([('new', 'New'), ('existing', 'Existing')], default='new')
    request_ids = fields.Many2many("approval.request", rel="approval_request_wizard", column1="wizard_id",
                                   column2="request_id", string="Approval Request")
    partner_id = fields.Many2one(comodel_name="res.partner", string="Customer")
    line_ids = fields.One2many("create.rfq.wizard.line", "wizard_id", string="Quotation Lines")
    # related_model = fields.Char(string="Related Model", store=True)
    # quotation_id = fields.Many2one(comodel_name="sale.order", string='Quotation')
    rfq_id = fields.Many2one(comodel_name="purchase.order", string='RFQ')
    approval_product_lines = fields.Many2many("approval.product.line", rel="approval_lines_wizard", column1="wizard_id",
                                              column2="line_id", string="Approval Lines")

    # @api.onchange('quotation_id', 'rfq_id')
    # def _get_action_type(self):
    #     for record in self:
    #         if record.quotation_id or record.rfq_id:
    #             if record.inquiry_screen_id.related_model == record.action_model:
    #                 record["action_type"] = 'existing'
    #             else:
    #                 record["action_type"] = 'new'
    #         else:
    #             record["action_type"] = 'new'

    @api.onchange('request_ids', 'only_lines', 'approval_product_lines')
    def fill_wizard_lines(self):
        """
        This function is an onchange method that is triggered when the fields 'request_ids', 'only_lines', or
         'approval_product_lines' are changed. It fills the wizard lines by creating 'create.rfq.wizard.line'
         records based on the selected request and product lines.

        :return: None
        """
        for record in self:
            for request_id in record.request_ids:
                for product_line in request_id.product_line_ids:
                    self.env["create.rfq.wizard.line"].create({
                        "product_id": product_line.product_id.id,
                        "name": product_line.description,
                        "product_uom_qty": 1,
                        "product_uom": product_line.product_id.uom_id.id,
                        "price_unit": product_line.last_purchase_price,
                        "wizard_id": record.id,
                        "product_line_id": product_line.id.origin
                    })
            if record.approval_product_lines:
                for product_line in record.approval_product_lines:
                    self.env["create.rfq.wizard.line"].create({
                        "product_id": product_line.product_id.id,
                        "name": product_line.description,
                        "product_uom_qty": 1,
                        "product_uom": product_line.product_id.uom_id.id,
                        "price_unit": product_line.last_purchase_price,
                        "wizard_id": record.id,
                        "product_line_id": product_line.id.origin
                    })

    def add_to_order(self):
        """
        Adds the current record to the order.

        :return: None
        """
        for record in self:
            if record.action_type == 'existing':
                order_id = record.rfq_id
            else:
                request_names = record.request_ids.mapped('name')
                order_id = self.env["purchase.order"].create({
                    "partner_id": record.partner_id.id,
                    "date_order": datetime.datetime.now(),
                    "company_id": self.env.company.id,
                    "user_id": self.env.user.id,
                    "origin": ','.join(request_names)
                })
            for line in record.line_ids:
                line_id = self.env["purchase.order.line"].create({
                    "product_id": line.product_id.id,
                    "name": line.name,
                    "product_qty": line.product_uom_qty,
                    "product_uom": line.product_uom.id,
                    "price_unit": line.price_unit,
                    "order_id": order_id.id,
                })
                # if record.request_ids.boq_id:
                #     line_id.account_analytic_id = record.request_ids.boq_id.project_id.analytic_account_id.id
                line.product_line_id["purchase_order_line_id"] = line_id.id


class AddToQuotationWizardLine(models.TransientModel):
    _name = 'create.rfq.wizard.line'
    _description = 'Create RFQ Wizard Line'

    product_id = fields.Many2one(comodel_name='product.product', string="Product")
    name = fields.Char(string="Description")
    product_uom_qty = fields.Float(string="Quantity")
    product_uom = fields.Many2one(comodel_name='uom.uom', string="UoM")
    price_unit = fields.Float(string="Unit Price")
    product_line_id = fields.Many2one(comodel_name="approval.product.line", string="Approval Product Line")
    wizard_id = fields.Many2one(comodel_name='create.rfq.wizard', string="Wizard")
