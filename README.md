# oxinus-spotlight

***1- Activities Customization:***

This customization serves to enhance security and data integrity within the 
activities feature of Odoo, ensuring that only certain users have the ability
to modify or delete records based on their roles and the record's creation history.

extends the mail.activity model.unlink method: Overrides the unlink method 
of the mail.activity model to introduce a security check. It prevents users
(except those with base.group_system, administrators) from deleting
activities created by other users.
In the view it adds a new field viewed_by_create_user which is invisible in the view.
It also makes the user_id field read-only based on the viewed_by_create_user 
field's value.

***2- Advanced CRM:***
This modules empowers the CRM module with advanced features and automations.
adding advanced automation, tracking, and user assignment features. 
It seems designed to enhance the user experience by automating repetitive tasks,
ensuring that opportunities are assigned and tracked effectively,
and providing a clear audit trail of user interactions with CRM records.

approval_request.py:
Extends the approval.request model from Odoo to add relations to CRM leads 
(crm.lead) and their stages (crm.stage).

**crm_lead.py:**
Extends the crm.lead model to add a variety of fields and methods for CRM automation.
It includes fields for tracking the opportunity's progress, the assignment to users,
and customer state.
Implements methods for creating approval requests, task creation, stage transitions,
and logging activities

**crm_lost_reason.py:**
Extends the crm.lost.reason model to add relations to email templates (mail.template),
allowing for different follow-up actions based on the reason a CRM 
opportunity is lost.

**crm_stage.py:**
Extends the crm.stage model to include additional configurations for CRM stages,
such as auto-progression to the next stage, activities to be scheduled at
each stage, and rules for dropping to another stage if certain conditions are met.

**crm_team.py:**
Extends the crm.team model to manage sales team rotations and rules for opportunity
assignments.

**project_task.py:**
Extends the project.task model to link tasks to CRM opportunities (crm.lead).

**sale.py:**
Extends the sale.order model to include a computed field that checks if a quotation
associated with a sales order is approved within the CRM workflow.

**view_logs.py:**
Defines a view.logs model for tracking views or interactions with various records
within the system, including the user who viewed the record and when.


***3- Commission System:***

Sales Commission System is a module that adds comprehensive sales commission tracking and calculation
functionality to the Odoo platform.

**achievement_incentives.py** file defines a model named achievement.incentives that is used for managing incentives for sales achievements within the commission system. It has fields to represent the target variable, achievement thresholds (from and to), and the amount or percent to reward.

**The duration_period.py** file defines a model named duration.period that is used to handle time periods for which sales commissions are calculated. It includes fields for the period name, start and end dates, and the year, along with a selection field for the duration (month, quarter, year).

**sale.py** file extends the sale.order.line model from the core sale module, adding a related field order_date to store when the order was placed.

**sales_commission_history.py** file defines a model for storing historical records of sales commissions. It includes fields for the associated sales team, the period of the commission, total achievement, and total amount rewarded. It also has a method to compute the total achievement and amount rewarded based on related detail records.

**sales_commission_history_details.py** file defines a model that appears to store the detailed line items for sales commission history. This model includes fields for the sales team, manager, member, and the amounts achieved and rewarded.

**sales_team.py** file extends the existing sales.team model from Odoo, adding a method to calculate commissions and a field to link to commission history records.

***4- Ziwo:***

The forge_ziwo module appears to integrate Ziwo, a cloud contact center solution, with Odoo. It adds phone communication capabilities directly into Odoo's CRM, Sales, and other modules.

**agent_screen.py:** Defines a transient model for the Agent Screen, which is a UI component for agents to manage calls, opportunities, tasks, tickets, and other records associated with a contact.

**crm_lead.py:** Extends the crm.lead model to include Ziwo-related functionalities, such as creating call records and linking them to leads.

**forge_sudo_override.py:** A model to override sudo with additional permissions or configurations.

**helpdesk_ticket.py:** Extends the helpdesk.ticket model to include phone number fields and Ziwo call history.

**mail_message.py:** Overrides the mail.message model to allow unsanitized HTML content, probably to include call recordings in messages.

**project_task.py:** Extends the project.task model to associate tasks with Ziwo call histories.

**res_config_settings.py:** Extends the res.config.settings model to include configurations for the Ziwo integration.

**res_partner.py:** Extends the res.partner model to manage contact information and ensures uniqueness of phone numbers.

**sale_order.py:** Extends the sale.order model to include Ziwo-related fields and functionalities.

**ziwo_active_form.py:** Defines a model to manage active forms that can be used in the Ziwo integration.

**ziwo_history.py:** Manages the history of calls made through Ziwo, including recording links, call types, and statuses.

**Three wizards** (AddNoteWizard, CreateCallWizard, and EditContactWizardThese) wizards enhance the user interaction 
with the Ziwo integration in Odoo by allowing efficient ways to add notes, create calls, and edit contact 
details directly from the interface, streamlining the communication and data management processes within Odoo. The
wizards are designed to be user-friendly and are integrated with the respective Odoo models to ensure seamless 
operation and data integrity.

***5- Multi-level Approvals:***

 Odoo module named "Multi-level Approvals", which extends the functionality of the standard Approvals module in Odoo to
 enable multi-level approval processes for various models like purchase orders, stock transfers, etc. 
 This module allows configuring detailed approval matrices and conditions, making the approval process more dynamic
 and adaptable to complex organizational structures.

**approval_matrix.py:** 
Defines an approval matrix model with fields like approver, approver group, lock record option, 
condition for the approval, approval type, and related model details. This model is used to configure the criteria and
approvers for each step of the multi-level approval process.

**create_rfq_wizard.py:**
A wizard to create RFQs (Request for Quotations) based on approval requests.
Includes fields for the partner, approval requests, lines for quotation, and the RFQ reference.
Methods to add lines to the order and create a new RFQ are defined.

**create_transfer_wizard.py:** 
A wizard for creating internal stock transfers based on approval requests.
Includes fields for operation type, destination location, and product lines.
Method create_internal_transfer to generate the stock picking (transfer) records.

**custom_approval_request.py:** 
Extends the approval.request model to include fields for related model, related
record ID, document reference, purchase type, related internal transfer, and transfer enablement.
Methods to check and create approvers, manage approval status, and create internal transfers or RFQs 
based on the approvals.

**custom_approval_types.py:** 
Extends the approval.category model to include additional fields for configuring approval
types, like sequential approval, condition approval, hierarchical approval, reset approval on update, etc.
Methods to manage how the approval process should behave based on these configurations.

**custom_approver.py:** 
Extends the approval.approver model to include a manager approval flag.
Methods to handle the approval logic, like approving all, checking the next approver, creating approvers, etc.

**purchase_approval_inherit.py:** 
Extends the purchase.order model to integrate the multi-level approval process into the purchase workflow.
Fields to track approval requests, status, and whether the purchase order requires approval.
Methods to send approval requests, confirm purchase orders, and create manager activities.

**settings_apply_approval.py:** 
A settings model to configure the multi-level approval feature within Odoo's settings.
Fields to enable multi-level approvals and set the default approval type for purchases.

***6- Oxinus Customizations:***

module oxinus_customization designed to extend and customize various aspects of the 
standard Odoo functionality. This module includes enhancements to CRM leads, sale orders, project tasks,
helpdesk tickets, and partner models.

**crm_lead.py:**
Extends the crm.lead model with additional KYC (Know Your Customer) fields, such as company brand names,
number of branches, delivery options, and device details.
Defines fields for SPOC (Single Point of Contact) details, including name, phone, email, language, and address.
Includes logic for manual dropping to a pool, navigating to the sales team, and handling changes in delivery options.

**crm_stage.py:**
Extends the crm.stage model to include a Boolean field to allow manual dropping of leads to a pool.

**crm_team.py:**
Enhances the crm.team model with a field to assign a lead account manager.

**helpdesk_ticket.py:**
Overrides the create method to set the user ID when a new ticket is created.

**kyc_device.py:**
Defines a new model for KYC devices with fields for device name, compatibility status, and a relation to CRM leads.

**project_task.py:**
Extends the project.task model with logic to copy messages and attachments from related sales orders upon task creation.
Includes related fields for KYC details and SPOC details from the associated sales order's opportunity.
Adds logic for stage change notifications and project manager notifications.

**project_task_type.py:**
Adds a Boolean field to the project.task.type model to indicate whether a stage is considered complete.

**res_partner.py:**
Extends the res.partner model with a Boolean field to indicate if a partner is an aggregator.

**sale_order.py:**
Extends the sale.order model with custom fields and logic.
Overrides the action_confirm method to assign an account manager if not already assigned.
Includes related KYC and SPOC fields from the associated CRM lead.
Adds custom logic to handle attachments separately from mail thread attachments.

***7- Subscription Customization:***

This module enhances the subscription and invoicing functionalities in Odoo by introducing additional controls and 
automation features. It allows for better management of invoice states, automated invoicing processes,
and customizations in the UI to accommodate these new features. It also offering more flexibility and automation in
handling subscriptions and related invoicing tasks.

**sale_order.py:** 
script from the provided Odoo module focuses on enhancing the functionality related to subscription management and 
invoicing within the SaleOrder and SaleOrderTemplate models.
