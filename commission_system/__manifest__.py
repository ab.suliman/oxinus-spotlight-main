# -*- coding: utf-8 -*-
{
    'name': "commission_system",

    'summary': """
        Sales Commission System""",

    'description': """
        Sales Commission System
    """,

    'author': "Forge Solutions",
    'website': "http://www.forge-solutions.com",
    'license': 'Other proprietary',

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Sale',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale', 'sales_team_roles'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/achievement_incentives_views.xml',
        'views/duration_period.xml',
        'views/sales_commission_history_views.xml',
        'views/sales_commission_history_details_views.xml',
        'views/variables_duration_views.xml',
        'views/variables_target_views.xml',
        'views/variables_views.xml',
        'views/commission_views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
