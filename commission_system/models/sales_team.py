# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime
from ast import literal_eval
import math

"""
     extends the existing sales.team model from Odoo, adding a method to calculate commissions and a field to link to commission history records.
    """


class SalesTeam(models.Model):
    _inherit = 'sales.team'

    sales_target = fields.Many2one(comodel_name="ranking.grades", string="Customer Grade", store=True,
                                   tracking=True)

    sales_commission_history = fields.One2many('sales.commission.history', 'sales_team', 'Commission History');

    def _calculate_commission(self):
        """
        Calculates the commission based on sales transactions and target achievements.
        It retrieves sales teams and their corresponding target variables, and then performs calculations based on
        the variable type. The code also creates commission history details and updates them if they already exist.

        Returns:
            None
        """
        sales_teams = self.env['sales.team'].search([])
        for team in sales_teams:
            variables_target = self.env['variables.target'].search([('sales_team', '=', team.id)])
            for variable_target in variables_target:
                variable = variable_target.variables_duration.variable_id
                target = variable_target.target_expected
                domain = variable.domain_rule
                variable_type = variable.variable_type
                date_field = variable.date_field_id.name
                start_date = variable_target.variables_duration.duration_start
                end_date = variable_target.variables_duration.duration_end
                if not literal_eval(domain):
                    domain = []
                else:
                    domain = literal_eval(domain)
                domain.append(['sales_team', '=', team.id])
                domain.append([date_field, '>=', start_date])
                domain.append([date_field, '<=', end_date])
                if variable_target.team_manager:
                    domain.append(
                        ['user_id', 'in', variable_target.team_manager.my_members.mapped("team_member").ids])
                elif variable_target.team_member:
                    domain.append(['user_id', '=', variable_target.team_member.id])

                transactions = self.env[variable.model_name].search(domain)
                # if domain:
                #     transactions = self.env[variable.model_name].filtered_domain(literal_eval(domain))
                # else:
                #     transactions = self.env[variable.model_name].search(
                #         [[date_field, '>=', start_date], [date_field, '<=', end_date]])
                if variable_type == 'sum':
                    total_achievement = sum(transactions.mapped(variable.field_id.name))
                if variable_type == 'avg':
                    if variable.field_id:
                        total_achievement = sum(transactions.mapped(variable.field_id.name)) / len(transactions)
                    else:
                        total_achievement = len(transactions) / len(transactions)
                if variable.variable_type == 'count':
                    total_achievement = len(transactions)
                achievement_incentives = self.env['achievement.incentives'].search(
                    [('variable_target', '=', variable_target.id)])
                for incentive in achievement_incentives:
                    achieved_percent = math.floor((total_achievement / target) * 100) / 100
                    if incentive.from_achievement <= achieved_percent <= incentive.to_achievement:
                        amount_to_reward = incentive.amount_to_reward
                        if incentive.percent_to_reward:
                            amount_to_reward += (incentive.percent_to_reward * total_achievement)
                        detail_created = self.env['sales.commission.history.details'].search(
                            [('sales_team', '=', team.id), ('variable_target_id', '=', variable_target.id)])
                        if not detail_created:
                            commission_history = self.env['sales.commission.history'].search(
                                [('sales_team', '=', team.id),
                                 ('variables_target_ids', 'in',
                                  variable_target.id)])
                            if not commission_history:
                                commission_history = self.env['sales.commission.history'].create({
                                    'sales_team': team.id,
                                    'duration_period': variable_target.variables_duration.id,
                                })
                            self.env['sales.commission.history.details'].create({
                                'sales_team': team.id,
                                'team_manager': variable_target.team_manager.id,
                                'team_member': variable_target.team_member.id,
                                'amount_achieved': total_achievement,
                                'amount_rewarded': float('%s%d' % (variable.variable_nature, amount_to_reward)),
                                'variable_target_id': variable_target.id,
                                'sales_commission_history_id': commission_history.id,
                                'percent_achieved': achieved_percent
                            })
                        else:
                            # if detail_created['points_rewarded'] != incentive.reward_points:
                            detail_created['amount_achieved'] = total_achievement
                            detail_created['percent_achieved'] = achieved_percent
                            detail_created['amount_rewarded'] = float(
                                '%s%d' % (variable.variable_nature, amount_to_reward))
