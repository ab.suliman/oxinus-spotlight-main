# -*- coding: utf-8 -*-

from odoo import models, fields, api

"""
    defines a model named sales.commission.history.details that appears to be used for storing
    sales commission history details. It has fields to represent the sales team, team manager,
    team member, amount achieved, percent achieved, amount rewarded, variable target, and
    sales commission history."""


class SalesCommissionHistoryDetails(models.Model):
    _name = 'sales.commission.history.details'
    _description = 'Sales Commission History Details'

    sales_team = fields.Many2one(string="Sales Team", comodel_name="sales.team", store=True)
    team_manager = fields.Many2one(string="Team Manager", comodel_name="res.users", store=True)
    team_member = fields.Many2one(string="Team Member", comodel_name="res.users", store=True)
    amount_achieved = fields.Float(string="Amount Achieved", store=True)
    percent_achieved = fields.Float(string="Percent Achieved", store=True)
    amount_rewarded = fields.Float(string="Amount Rewarded", store=True)
    variable_target_id = fields.Many2one(string="Variables Target",
                                         comodel_name="variables.target", store=True)
    sales_commission_history_id = fields.Many2one(string="Sales Commission History",
                                                  comodel_name="sales.commission.history",
                                                  store=True)

    # @api.depends('date')
    # def _get_date_month(self):
    #     if self.date:
    #         self.date_month = self.date.month()
