# -*- coding: utf-8 -*-


"""
    defines a model named achievement.incentives that appears to be used for managing incentives
    for sales achievements within the commission system. It has fields to represent the target
    variable, achievement thresholds (from and to), and the amount or percent to reward.
"""
from odoo import models, fields, api


class AchievementIncentives(models.Model):
    _name = 'achievement.incentives'
    _description = 'Achievement Incentives'

    variable_target = fields.Many2one(string="Variable Target", comodel_name="variables.target", store=True)
    from_achievement = fields.Float(string="From Achievement", widget="percentage", store=True)
    to_achievement = fields.Float(string="To Achievement", widget="percentage", store=True)
    amount_to_reward = fields.Float(string="Amount To Reward", store=True)
    percent_to_reward = fields.Float(string="Percent To Reward", store=True)