# -*- coding: utf-8 -*-

from odoo import models, fields, api

"""
    defines a model named variables.target that appears to be used for storing variables
    within the commission system. It has fields to represent the variable name, variables_duration,
    target_expected, and sales_team, sales_team_users, sales_team_managers, team_manager, and team_member.
    """


class VariablesTarget(models.Model):
    _name = 'variables.target'
    _description = 'Variables Target'

    name = fields.Char(string="Target", compute="_get_name")
    variables_duration = fields.Many2one(string="Variables Duration", comodel_name="variables.duration", store=True)
    target_expected = fields.Float(string="Target Expected", store=True)
    sales_team = fields.Many2one(string="Sales Team", comodel_name="sales.team", store=True)
    sales_team_users = fields.Many2many(related="sales_team.team_users")
    sales_team_managers = fields.Many2many(related="sales_team.team_managers")
    team_manager = fields.Many2one(string="Team Manager", comodel_name="res.users", store=True)
    team_member = fields.Many2one(string="Team Member", comodel_name="res.users", store=True)

    @api.depends('variables_duration', 'sales_team', 'target_expected')
    def _get_name(self):
        """
        A function that calculates the name based on the values of the 'variables_duration', 'sales_team', and 'target_expected' fields.

        Parameters:
            self (obj): The record(s) to calculate the name for.

        Returns:
            None
        """
        for record in self:
            record["name"] = '(' + record["sales_team"].name + ') ' + record["variables_duration"].name + ': ' + str(
                record[
                    "target_expected"])
