# -*- coding: utf-8 -*-

from odoo import models, fields, api

"""extends the sale.order.line model from the core sale module, adding a related field order_date to store when the order was placed."""


class Sale(models.Model):
    _inherit = 'sale.order.line'

    order_date = fields.Datetime(related="order_id.date_order", store=True)
