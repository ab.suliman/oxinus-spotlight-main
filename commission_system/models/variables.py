# -*- coding: utf-8 -*-

from odoo import models, fields, api

"""
    defines a model named variables that appears to be used for storing variables
    within the commission system. It has fields to represent the variable name,
    variable model, variable field, date field, domain rule, variable type, and
    variable nature.
    """


class Variables(models.Model):
    _name = 'variables'
    _description = 'Variables'

    name = fields.Char(string="Variable Name", store=True)
    model_id = fields.Many2one(string="Variable Model", comodel_name="ir.model", store=True)
    model_name = fields.Char(string="Variable Model Name", related="model_id.model", store=True)
    field_id = fields.Many2one(string="Variable Field", comodel_name="ir.model.fields", store=True)
    date_field_id = fields.Many2one(string="Date Field", comodel_name="ir.model.fields", store=True)
    domain_rule = fields.Char(string="Domain", store=True)
    variable_type = fields.Selection(selection=[('sum', 'Sum'), ('count', 'Count'), ('avg', 'Average')])
    variable_nature = fields.Selection(selection=[('+', '+'), ('-', '-')])
