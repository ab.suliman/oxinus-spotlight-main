# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError

"""
    defines a model named duration.period that seems to handle time periods for which sales commissions
    are calculated. It includes fields for the period name, start and end dates, and the year, along 
    with a selection field for the duration (month, quarter, year).
"""


class DurationPeriod(models.Model):
    _name = 'duration.period'
    _description = 'Duration Period'

    name = fields.Char(string="Period Name", compute="_get_duration_name")
    start_date = fields.Date(string="Start Date", store=True, required=1)
    end_date = fields.Date(string="End Date", store=True, required=1)
    year = fields.Char(string="Year", compute="_get_year", readonly=1)
    duration = fields.Selection(selection=[('month', 'Month'), ('quarter', 'Quarter'), ('year', 'Year')])

    @api.constrains('start_date', 'end_date')
    def date_constrains(self):
        """
        This function is a constraint that checks the validity of the 'start_date' and 'end_date' fields.

        :param self: The current recordset.
        :return: None

        This function loops over the records in the current recordset and performs the following checks:
        1. If both 'start_date' and 'end_date' are set, it checks if 'end_date' is less than or equal to 'start_date'. If so, it raises a validation error.
        2. If 'start_date' is set, it checks if it is less than the current date. If so, it raises a validation error.
        3. If 'end_date' is set, it checks if it is less than or equal to the current date. If so, it raises a validation error.
        """
        for rec in self:
            if rec.start_date and rec.end_date:
                if rec.end_date <= rec.start_date:
                    raise ValidationError(_("End Date must be greater than Start Date."))
            if rec.start_date:
                if rec.start_date < fields.Date.today():
                    raise ValidationError(_("Start Date must be greater than today date."))
            if rec.end_date:
                if rec.end_date <= fields.Date.today():
                    raise ValidationError(_("End Date must be greater than today date."))

    @api.depends('start_date', 'end_date', 'duration', 'year')
    def _get_duration_name(self):
        """
        Updates the "name" field of each record with a formatted duration name.

        This function takes into account the values of the "start_date", "end_date", "duration", and "year" fields
        to determine the appropriate value for the "name" field of each record.

        Parameters:
            self (Model): The current recordset.

        Returns:
            None
        """
        for record in self:
            record["name"] = "New"
            if record["duration"] == 'month':
                if record["start_date"] and record["year"]:
                    record["name"] = record["start_date"].strftime("%B") + ' ' + record["year"] + ' (' + record[
                        "duration"] + ')'
            else:
                if record["start_date"] and record["end_date"] and record["year"] and record["duration"]:
                    record["name"] = record["start_date"].strftime("%B") + ' - ' + record["end_date"].strftime(
                        "%B") + ' ' + \
                                     record[
                                         "year"] + ' (' + record["duration"] + ')'

    @api.depends('start_date')
    # @api.onchange('start_date')
    def _get_year(self):
        """
        Calculate the year based on the start date.

        :param self: The current recordset.
        :type self: Recordset
        :return: None
        """
        for record in self:
            record["year"] = ""
            if record["start_date"]:
                record["year"] = record["start_date"].year
