# -*- coding: utf-8 -*-

from odoo import models, fields, api

"""
    defines a model named sales.commission.history that represents the sales commission history
    for a sales team. It has fields to represent the sales team, date, date month, duration period,
    achievement, total amount rewarded, variables target, and sales commission history details.
    It also has a method to compute the total achievement and amount rewarded based on related detail records.
 """


class SalesCommissionHistory(models.Model):
    _name = 'sales.commission.history'
    _description = 'Sales Commission History'

    sales_team = fields.Many2one(string="Sales Team", comodel_name="sales.team", store=True, readonly=1)
    # date = fields.Date(string="Date", store=True)
    # date_month = fields.Integer(string="Date Month", store=True)
    duration_period = fields.Many2one(string="Duration Period", comodel_name="duration.period", store=True, readonly=1)
    achievement = fields.Float(string="Achievement", compute="_get_total_achievement", readonly=1)
    total_amount_rewarded = fields.Float(string="Total Amount Rewarded", readonly=1,
                                         compute="_get_total_amount_rewarded")
    variables_target_ids = fields.Many2many(string="Variables Target", rel="variables_target_history_rel",
                                            column1="history_id", column2="variable_target_id",
                                            comodel_name="variables.target", store=True,
                                            compute="_get_variable_targets")
    sales_history_details = fields.One2many('sales.commission.history.details', 'sales_commission_history_id',
                                            string='Sales Commission History Details',
                                            store=True, readonly=True)

    # @api.depends('date')
    # def _get_date_month(self):
    #     if self.date:
    #         self.date_month = self.date.month()

    @api.depends('sales_history_details')
    def _get_total_achievement(self):
        """
        Compute and set the total achievement for each record.

        This function takes no parameters.

        Returns:
            None
        """
        for record in self:
            record['achievement'] = sum(record["sales_history_details"].mapped("amount_achieved"))

    @api.depends('sales_history_details')
    def _get_total_amount_rewarded(self):
        """
        Calculates the total amount rewarded for each record.

        This function iterates through each record in the current object and calculates
        the total amount rewarded by summing the "amount_rewarded" values from the
        related "sales_history_details" records. The calculated total is then assigned
        to the "total_amount_rewarded" field of each record.

        Parameters:
            self (RecordSet): The current object containing the records to process.

        Returns:
            None
        """
        for record in self:
            record['total_amount_rewarded'] = sum(record["sales_history_details"].mapped("amount_rewarded"))

    @api.depends('sales_history_details')
    def _get_variable_targets(self):
        for record in self:
            """
            This function is used to calculate the variable targets for each record in the current instance of the class.

            Parameters:
                self (object): The current instance of the class.

            Returns:
                None
            """
            record.variables_target_ids = [(6, 0, [])]
            for history_detail in record.sales_history_details:
                record.variables_target_ids = [(4, history_detail.variable_target_id.id)]

            # record.total_rewarded_points = points
            #
            # customer_ranking = record.env['customer.ranking'].search(
            #     [('from_points', '<=', points), ('to_points', '>=', points)])
            #
            # record.from_grade = self.partner_id.customer_grade_id.id
            # record.to_grade = customer_ranking.grade_id.id
            # record.partner_id.customer_grade_id = customer_ranking.grade_id.id
