# -*- coding: utf-8 -*-

from odoo import models, fields, api

"""
    defines a model named variables.duration that appears to be used for storing variables
    within the commission system. It has fields to represent the variable name, duration_period, duration_start
    duration_end, and variable_id.
"""


class VariablesDuration(models.Model):
    _name = 'variables.duration'
    _description = 'Variables Duration'

    name = fields.Char(string="Variable Duration Name", compute="_get_name")
    duration_period = fields.Many2one(string="Duration Period", comodel_name="duration.period", store=True)
    duration_start = fields.Date(string="Duration Start", related="duration_period.start_date", store=True)
    duration_end = fields.Date(string="Duration End", related="duration_period.end_date", store=True)
    variable_id = fields.Many2one(string="Variable", comodel_name="variables", store=True)

    @api.depends('duration_period', 'variable_id')
    def _get_name(self):
        """
        Calculate the value for the 'name' field based on the 'duration_period' and 'variable_id' fields.

        :return: None
        """
        for record in self:
            record["name"] = "New"
            if record["variable_id"] and record["duration_period"]:
                record["name"] = record["variable_id"].name + ' (' + record["duration_period"].name + ')'
